<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/11
 * Time: 19:34
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——编辑管理员</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="index.php?c=admin-logout">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="index.php?c=category-list">分类管理</a></li>
            <li><a href="index.php?c=article-list">文章管理</a></li>
            <li><a href="index.php?c=admin-list">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=article-list">首页</a>&gt;
                <a href="index.php?c=admin-list">管理员管理</a>&gt;
                <a href="index.php?c=admin-list">管理员列表</a>&gt;
                <a href="index.php?c=admin-list">编辑管理员</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="index.php?c=admin-edit-save" method="post">
                    <table>
                        <tr>
                            <td>邮箱：</td>
                            <td><input type="text" name="admin_email" value="<?php echo $adminInfo['admin_email'] ?>" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>姓名：</td>
                            <td><input type="text" name="admin_name" value="<?php echo $adminInfo['admin_name'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>密码：</td>
                            <td><input type="password" name="admin_password" value="<?php echo $adminInfo['admin_password'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>确认密码：</td>
                            <td><input type="password" name="again_password"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>

<?php
/**后台——编辑分类保存页面
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条分类信息到数据库
$sql = "update category 
          set category_name = '$categoryName',category_desc = '$categoryDesc',update_time = '$updateTime'
          where category_id = '$categoryId'";
$result = $db->exec($sql);

if($result){
    //编辑分类成功日志
    $log =[
        'category_id' => $categoryId,
        'category_name' => $categoryName,
        'category_desc' => $categoryDesc,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'edit',
        'content' => '编辑分类成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logCategoryEditSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logCategoryEditSuccess.PHP_EOL,FILE_APPEND);

}
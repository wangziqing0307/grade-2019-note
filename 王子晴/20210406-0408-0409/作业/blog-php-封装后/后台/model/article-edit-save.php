<?php
/**后台——编辑文章保存页面
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条文章信息到数据库
$sql = "update article 
          set article_title = '$articleTitle',category_id = '$categoryId',article_author = '$articleAuthor',article_content = '$articleContent',update_time = '$updateTime'
          where article_id = '$articleId'";
$result = $db->exec($sql);

if($result){
    //编辑文章成功日志
    $log = [
        'article_id' => $articleId,
        'article_title' => $articleTitle,
        'category_id' => $categoryId,
        'article_author' => $articleAuthor,
        'article_content' => $articleContent,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'edit',
        'content' => '编辑文章成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logArticleEditSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logArticleEditSuccess.PHP_EOL,FILE_APPEND);

}
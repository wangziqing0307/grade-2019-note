<?php
/**
 * 后台——文章批量删除保存页面
 */

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//删除一条或多条文章信息到数据库
$sql = "delete from article where article_id in (".implode(",",$articleIds).")";
$result = $db->exec($sql);
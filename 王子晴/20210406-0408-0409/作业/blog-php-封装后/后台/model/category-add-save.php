<?php
/**
 * 后台——增加分类保存页面
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//增加一条分类信息到数据库
$sql = "insert into category (category_name,category_desc,add_time,update_time) 
          values ('$categoryName','$categoryDesc','$addTime','$updateTime')";
$result = $db->exec($sql);

if($result){
    //增加分类成功日志
    $log = [
        'category_name' => $categoryName,
        'category_desc' => $categoryDesc,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'add',
        'content' => '增加分类成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logCategoryAddSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logCategoryAddSuccess.PHP_EOL,FILE_APPEND);
}
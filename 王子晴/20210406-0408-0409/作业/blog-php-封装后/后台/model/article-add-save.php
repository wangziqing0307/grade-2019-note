<?php
/**
 * 后台——增加文章保存页面
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//增加一条文章信息到数据库
$sql = "insert into article (article_title,category_id,article_author,article_content,add_time,update_time) 
          values ('$articleTitle','$categoryId','$articleAuthor','$articleContent','$addTime','$updateTime')";
$result = $db->exec($sql);

if($result){
    //增加文章成功日志
    $log = [
        'article_title' => $articleTitle,
        'category_id' => $categoryId,
        'article_author' => $articleAuthor,
        'article_content' => $articleContent,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'add',
        'content' => '增加文章成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logArticleAddSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logArticleAddSuccess.PHP_EOL,FILE_APPEND);

}
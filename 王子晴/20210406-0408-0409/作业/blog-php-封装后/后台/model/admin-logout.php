<?php
/**
 * 后台——管理员登出
 */
//删除session
unset($_SESSION['admin_email']);
unset($_SESSION['admin_name']);

//记录登出日志
if(empty($_SESSION['admin_email'])){
    $log = [
        'admin_email' => null,
        'admin_name' => null,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'logout',
        'content' => '登出后台成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLogoutSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLogoutSuccess.PHP_EOL,FILE_APPEND);
}
<?php
/**
 *管理员登录保存页面
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

//查询该用户是否存在且信息正确
$sql = "select * from adminInfo where admin_email = '$adminEmail'";
$result = $db->query($sql);

if(!$result){
    //记录登录失败日志
    $errorInfo = $db->errorInfo()[2];
    $log = [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        'content' => '登录后台失败',
        'errorInfo' => $errorInfo,
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLoginError = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLoginError.PHP_EOL,FILE_APPEND);
}

//$errorInfo = $db->errorInfo()[2];
//print_r($errorInfo);

$adminInfo = $result->fetch(PDO::FETCH_ASSOC);

//将用户登录的密码进行md5加密
$salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
$adminPassword = md5($salt.md5($salt.$adminPassword.$salt).$salt);

if($adminInfo && $adminPassword === $adminInfo['admin_password']){
    //登录成功后，设置相关信息
    if(!session_id()){
        session_start();
    }
    $_SESSION['admin_email'] = $adminEmail;
    $_SESSION['admin_name'] = $adminInfo['admin_name'];

    //点击了记住我，设置过期时间一天后
    if($isRemember){
        setcookie('PHPSESSID',session_id(),time()+10);
    }

    //记录登录成功日志
    $log = [
        'admin_email' => $_SESSION['admin_email'],
        'admin_name' => $_SESSION['admin_name'],
        'ip' => $_SERVER['REMOTE_ADDR'],        //ip地址
        'action' => 'login',
        'content' => '登录后台成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLoginSuccess= json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLoginSuccess.PHP_EOL,FILE_APPEND);
}else{
    //记录登录失败日志
    if(empty($adminInfo)){
        $errorInfo = '数据库查找不到';
    }else if(empty($adminPassword)){
        $errorInfo = "密码为空";
    }else if($adminInfo['admin_password'] !== $adminPassword){
        $errorInfo = '密码错误';
    }
    $log = [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        'content' => '登录后台失败',
        'errorInfo' => $errorInfo,
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLoginError = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLoginError.PHP_EOL,FILE_APPEND);

}
<?php
/**
 * 后台——文章批量删除保存页面
 */

//获取一个或多个文章id
$articleIds = $_POST['article'];

include_once APP_PATH."./model/article-delete-multi.php";
include_once APP_PATH."./view/article-delete-multi.php";
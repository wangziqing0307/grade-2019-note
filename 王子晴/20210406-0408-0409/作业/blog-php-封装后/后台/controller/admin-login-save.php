<?php
/**
 *管理员登录保存页面
 */
//接收到管理员登录信息
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$isRemember = $_POST['isRemember'] ?? '';
$adminNum = $_POST['admin_randomNum'];

//判断验证码是否一致
session_start();

if(!($adminNum == $_SESSION['checkNum'])){
    echo "验证码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

include_once APP_PATH."./model/admin-login-save.php";
include_once APP_PATH."./view/admin-login-save.php";
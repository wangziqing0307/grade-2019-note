<?php
/**
 *后台——增加管理员保存页面
 */
//接收增加管理员信息
$adminName = $_POST['admin_name'];
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$againPassword = $_POST['again_password'];

//邮箱5~100长度
if(mb_strlen($adminEmail)<5 || mb_strlen($adminEmail)>100){
    echo "邮箱长度5~100<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//姓名2~30长度
if(mb_strlen($adminName)<2 || mb_strlen($adminName)>30){
    echo "姓名长度2~30<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//密码6~20长度
if((mb_strlen($adminPassword)<6 || mb_strlen($adminPassword)>20) && (mb_strlen($againPassword)<6 || mb_strlen($againPassword)>20)){
    echo "密码长度6~20<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断密码是否一致
if(!($adminPassword == $againPassword)){
    echo "密码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

//对密码进行md5加密
$salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
$adminPassword = md5($salt.md5($salt.$adminPassword.$salt).$salt);

include_once APP_PATH."./model/admin-add-save.php";
include_once APP_PATH."./view/admin-add-save.php";
<?php
/**
 * 后台——管理员登出
 */
//开启会话
session_start();

include_once APP_PATH."./model/admin-logout.php";
include_once APP_PATH."./view/admin-logout.php";
<?php
/**
后台——分类删除
 */
//获取分类id
$categoryId = $_GET['category_id'];

//表单验证(后台管理功能细节)
//分类id不能为空
if(empty($categoryId)){
    echo "分类id不能为空<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

include_once APP_PATH."./model/category-delete.php";
include_once APP_PATH."./view/category-delete.php";


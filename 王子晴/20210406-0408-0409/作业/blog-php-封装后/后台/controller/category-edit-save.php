<?php
/**后台——编辑分类保存页面
 */
//获取到分类信息
$categoryId = $_POST['category_id'];
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];

//表单验证(后台管理功能细节)
//分类id不能为空
if(empty($categoryId)){
    echo "分类id不能为空<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//分类名称2~45个字
if(mb_strlen($categoryName)<2 || mb_strlen($categoryName)>45){
    echo "分类名称2~45个字符<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//分类说明10~255个字
if(mb_strlen($categoryDesc)<10 || mb_strlen($categoryDesc)>255){
    echo "分类说明10~255个字符<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

include_once APP_PATH."./model/category-edit-save.php";
include_once APP_PATH."./view/category-edit-save.php";
<?php
/**
后台——分类编辑
 */
//开启会话
session_start();

if(empty($_SESSION['admin_email'])){
    echo "尚未登录，<br/>请<a href='admin-login.php'>前往登录页面</a>,重新登录";
    exit();
}

//获取到分类id
$categoryId = $_GET['category_id'];

include_once APP_PATH."./model/category-edit.php";
include_once APP_PATH."./view/category-edit.php";
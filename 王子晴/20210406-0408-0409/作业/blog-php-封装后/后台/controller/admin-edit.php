<?php
/**
 * 后台——编辑管理员
 */
//开启会话
session_start();

if(empty($_SESSION['admin_email'])){
    echo "尚未登录，<br/>请<a href='admin-login.php'>前往登录页面</a>,重新登录";
    exit();
}

//接受管理员id
$adminId = $_GET['admin_id'];

include_once APP_PATH."./model/admin-edit.php";
include_once APP_PATH."./view/admin-edit.php";
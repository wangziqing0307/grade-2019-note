<?php
/**
后台——分类列表
 */
//开启会话
session_start();

if(empty($_SESSION['admin_email'])){
    echo "尚未登录，<br/>请<a href='admin-login.php'>前往登录页面</a>,重新登录";
    exit();
}

include_once APP_PATH."./model/category-list.php";
include_once APP_PATH."./view/category-list.php";
<?php
/**
 * 多行读取
 */
//开启文件，以只读的方式
$file = fopen("log.txt","r");

//判断是否开启文件成功
//法一
if($file){
    //多行读取文件
    while (true){
        $str = fgets($file);
        echo $str."<br/>";
        if($str === false){
            break;
        }
    }
}else{
    echo "开启文件错误";
}

//法二、feof()——判断是否到达文件末尾（eof）
if($file){
    while (!feof($file)){
        $str = fgets($file);
        echo $str."<br/>";
    }
}else{
    echo "开启文件错误";
}
<?php
/**
 * 读取文件
 */
//开启文件，以只读的方式
$file = fopen("log.txt","r");
//按长度读取文件
echo fread($file,1)."<br/>";         //输出h
echo fread($file,5)."<br/>";                 //输出ello

echo fgetc($file)."<br/>";              //输出p（c相当于char一个字母）

//按行读取文件
echo fgets($file)."<br/>";              //输出hp file+一个空格（空格表示换行）（s相当于string字符串）

//尝试多行读取文件
echo fgetss($file);         //报错，Deprecated——可以使用但不推荐
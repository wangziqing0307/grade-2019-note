<?php
/**
 * 写入文件
 */
//开启文件,以只写的方式
$file = fopen('log.txt',"w");
//判断是否开启文件成功
if($file){
    //写入文件内容（单行,覆盖）
    fwrite($file,"hello php file");
    echo '写入文件内容success<br/>';
    //关闭文件
    fclose($file);
}else{
    echo '开启文件失败';
}

//开启文件,以追加的方式
$file = fopen('log.txt',"a+");
//判断是否开启文件成功
if($file){
    //写入文件内容（多行）
    fwrite($file,"\r\nhello php file");
    fwrite($file,PHP_EOL."hello php file");
    echo '写入文件内容success';
    //关闭文件
    fclose($file);
}else{
    echo '开启文件失败';
}

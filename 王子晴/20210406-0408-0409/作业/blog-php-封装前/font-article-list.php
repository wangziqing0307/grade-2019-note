<?php
/**
前台——文章列表
 */
//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//查询文章数量
$sql = "select count(*) as total from article ";
$result = $db->query($sql);
$articleList = $result->fetch(PDO::FETCH_ASSOC);

$totalNum = $articleList['total'];
$totalPage = ceil($totalNum/3);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>前台——首页</title>
    <link rel="stylesheet" type="text/css" href="css/font-base.css" />
</head>
<body>
<div id="big">
    <!--头部开始-->
    <div>
        <div id="header_top">
            <b>非常日记</b>
            <ul><li>一个有灵魂的博客</li></ul>
        </div>
        <div id="header_bottom">
            <ul>
                <li><b><a href="#" class="header_a">首页</a></b></li>
                <li><b><a href="#" class="header_a">html</a></b></li>
                <li><b><a href="#" class="header_a">css</a></b></li>
                <li><b><a href="#" class="header_a">数据库</a></b></li>
            </ul>
        </div>
    </div>
    <!--头部结束-->
    <!--中间部分开始-->
    <div id="middle">
        <div class="middle_title">
            <h3>vue入门介绍</h3>
            <p><b>2021-03-01 15:00:00</b></p>
        </div>
        <ul>
            <li>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</li>
            <li><div><a href="#">阅读全文</a></div></li>
        </ul>
        <div class="middle_title">
            <h3>vue入门介绍</h3>
            <p><b>2021-03-01 15:00:00</b></p>
        </div>
        <ul>
            <li>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</li>
            <li><div><a href="#">阅读全文</a></div></li>
        </ul>
        <div class="middle_title">
            <h3>vue入门介绍</h3>
            <p><b>2021-03-01 15:00:00</b></p>
        </div>
        <ul>
            <li>Vue (读音 /vjuː/，类似于 view) 是一套用于构建用户界面的渐进式框架。与其它大型框架不同的是，Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层，不仅易于上手，还便于与第三方库或既有项目整合。</li>
            <li><div><a href="#">阅读全文</a></div></li>
        </ul>
        <div id="middle_bottom">
            <ul>
                <li class="category" id="prev"><a>上一页</a></li>
                <li><a href="font-article-list.php?page=1">1</a></li>
                <li><a href="font-article-list.php?page=2">2</a></li>
                <li><a href="font-article-list.php?page=3">3</a></li>
                <li><a href="font-article-list.php?page=4">4</a></li>
                <li><a href="font-article-list.php?page=5">5</a></li>
                <li class="category" id="next"><a>下一页</a></li>
            </ul>
        </div>
    </div>
    <!--中间部分结束-->
    <!--底部开始-->
    <div id="bottom">
        <ul><li>copyright @ 2021 非常日记</li></ul>
    </div>
    <!--底部结束-->
</div>
</body>
</html>



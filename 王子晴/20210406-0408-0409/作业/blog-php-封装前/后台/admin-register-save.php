<?php
/**
 * 管理员注册保存页面页面
 */

//接收管理员注册信息
$adminName = $_POST['admin_name'];
$adminPassword = $_POST['admin_password'];
$againPassword = $_POST['again_password'];

//姓名非空
if(empty($adminName)){
    echo "姓名为空，请重新输入<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//密码8~15个字符
if((mb_strlen($adminPassword)<8 || mb_strlen($adminPassword)>15) && (mb_strlen($againPassword)<8 || mb_strlen($againPassword)>15)){
    echo "密码8~15个字符<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断密码是否一致
if(!($adminPassword == $againPassword)){
    echo "密码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;


//插入一条管理员注册信息
$sql = "insert into adminInfo (admin_name,admin_password,add_time,update_time) 
          values ('$adminName','$adminPassword','$addTime',$updateTime)";
$result = $db->exec($sql);

//判断是否管理员注册信息成功
if($result){
    echo '管理员注册信息成功<br/><a href="admin-login.php">返回管理员登录页</a>';
    exit();
}else{
    echo '管理员注册信息失败，错误信息：'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}

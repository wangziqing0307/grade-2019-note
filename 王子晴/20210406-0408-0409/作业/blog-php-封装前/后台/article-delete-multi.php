<?php
/**
 * 后台——文章批量删除保存页面
 */
//获取一个或多个文章id
$articleIds = $_POST['article'];


//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//删除一条或多条文章信息到数据库
$sql = "delete from article where article_id in (".implode(",",$articleIds).")";
$result = $db->exec($sql);

//判断是否批量成功
if($result){
    echo '批量删除文章信息成功<br/><a href="font-article-list.php">返回文章列表首页</a>';
    exit();
}else{
    echo '批量删除文章信息失败，错误信息：'.$db->errorInfo()[2].'.请联系管理员：2270467108@qq.com';
}
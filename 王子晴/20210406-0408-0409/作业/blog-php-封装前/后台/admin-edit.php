<?php
/**
 * 后台——编辑管理员
 */

//开启会话
session_start();

//接受管理员id
$adminId = $_GET['admin_id'];

//设置时区
date_default_timezone_set("PRC");

//连接到MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

////查询管理员表
//$sql = "select * from adminInfo";
//$result = $db->query($sql);
//$adminInfoList = $result->fetch(PDO::FETCH_ASSOC);

//查询一条管理员信息
$sql = "select * from adminInfo where admin_id = '$adminId' ";
$result = $db->query($sql);
$adminInfo = $result->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——编辑管理员</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="admin-logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category-list.php">分类管理</a></li>
            <li><a href="article-list.php">文章管理</a></li>
            <li><a href="admin-list.php">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article-list.php">首页</a>&gt;
                <a href="admin-list.php">管理员管理</a>&gt;
                <a href="admin-list.php">管理员列表</a>&gt;
                <a href="admin-list.php">编辑管理员</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="admin-edit-save.php" method="post">
                    <table>
                        <tr>
                            <td>邮箱：</td>
                            <td><input type="text" name="admin_email" value="<?php echo $adminInfo['admin_email'] ?>" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>姓名：</td>
                            <td><input type="text" name="admin_name" value="<?php echo $adminInfo['admin_name'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>密码：</td>
                            <td><input type="password" name="admin_password" value="<?php echo $adminInfo['admin_password'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>确认密码：</td>
                            <td><input type="password" name="again_password"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>


<?php
/**
后台——分类列表
 */

//开启会话
session_start();

date_default_timezone_set("PRC");

$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$sql = "select * from category order by category_id asc ";
$result = $db->query($sql);
//var_dump($db->errorInfo());
//exit();
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——分类列表</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="admin-logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category-list.php">分类管理</a></li>
            <li><a href="article-list.php">文章管理</a></li>
            <li><a href="admin-list.php">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article-list.php">首页</a>&gt;
                <a href="category-list.php">分类管理</a>&gt;
                <a href="category-list.php">分类列表</a>
            </div>
            <div id="table-menu">
<!--                <button class="btn">全选</button>-->
<!--                <a href="#">删除选中任务</a>-->
                <a href="category-add.php" id="add_article">增加分类</a>
            </div>
            <div class="table-list" id="list">
                <form action="#" method="post">
                    <table>
                        <tr>
                            <th id="fist_th"></th>
                            <th>分类id</th>
                            <th>分类名称</th>
                            <th>分类描述</th>
                            <th>增加时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach ($categoryList as $row): ?>
                        <tr>
                            <td><input type="checkbox" id="table_checkbox" /></td>
                            <td><?php echo $row['category_id'] ?></td>
                            <td><?php echo $row['category_name'] ?></td>
                            <td><?php echo $row['category_desc'] ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['add_time']) ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['update_time']) ?></td>
                            <td>
                                <a href="category-edit.php?category_id=<?php echo $row['category_id'] ?>">编辑</a>
                                <a href="category-delete.php?category_id=<?php echo $row['category_id'] ?>">删除</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>



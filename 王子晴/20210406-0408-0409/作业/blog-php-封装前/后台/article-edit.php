<?php
/**
后台——文章编辑
 */

//开启会话
session_start();

//获取到文章id
$articleId = $_GET['article_id'];


//设置时区
date_default_timezone_set("PRC");

//连接到MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//查询分类表
$sql = "select * from category";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);

//查询一条文章信息
$sql = "select * from article where article_id = '$articleId' ";
$result = $db->query($sql);
$article = $result->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——编辑文章</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="admin-logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category-list.php">分类管理</a></li>
            <li><a href="article-list.php">文章管理</a></li>
            <li><a href="admin-list.php">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article-list.php">首页</a>&gt;
                <a href="article-list.php">文章管理</a>&gt;
                <a href="article-list.php">文章列表</a>&gt;
                <a href="article-edit.php">编辑文章</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="article-edit-save.php" method="post">
                    <table>
                            <input type="hidden" name="article_id" value="<?php echo $article['article_id'] ?>"/>
                        <tr>
                            <td>文章标题：</td>
                            <td><input type="text" name="article_title" value="<?php echo $article['article_title'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>文章分类：</td>
                            <td>
                                <select name="category_id">
                                    <?php foreach ($categoryList as $item): ?>
                                    <option value="<?php echo $item['category_id'] ?>" <?php echo $article['category_id']==$item['category_id'] ? 'selected="selected"': ''; ?>>
                                        <?php echo $item['category_name']; ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>文章作者：</td>
                            <td><input type="text" name="article_author" value="<?php echo $article['article_author'] ?>" /></td>
                        </tr>
                        <tr>
                            <td>文章内容：</td>
                            <td><textarea name="article_content"><?php echo $article['article_content'] ?></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>

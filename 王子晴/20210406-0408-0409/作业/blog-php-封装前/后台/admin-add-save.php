<?php
/**
 *后台——增加管理员保存页面
 */
//接收增加管理员信息
$adminName = $_POST['admin_name'];
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$againPassword = $_POST['again_password'];

//邮箱5~100长度
if(mb_strlen($adminEmail)<5 || mb_strlen($adminEmail)>100){
    echo "邮箱长度5~100<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//姓名2~30长度
if(mb_strlen($adminName)<2 || mb_strlen($adminName)>30){
    echo "姓名长度2~30<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//密码6~20长度
if((mb_strlen($adminPassword)<6 || mb_strlen($adminPassword)>20) && (mb_strlen($againPassword)<6 || mb_strlen($againPassword)>20)){
    echo "密码长度6~20<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断密码是否一致
if(!($adminPassword == $againPassword)){
    echo "密码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

//对密码进行password_hash加密
$adminPasswordHash = password_hash($adminPassword,PASSWORD_BCRYPT);

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;


//插入一条管理员注册信息
$sql = "insert into adminInfo (admin_name,admin_email,admin_password,add_time,update_time) 
          values ('$adminName','$adminEmail','$adminPasswordHash','$addTime',$updateTime)";
$result = $db->exec($sql);

//判断是否管理员注册信息成功
if($result){
    echo '增加管理员信息成功<br/><a href="admin-list.php">返回管理员列表页</a>';
    exit();
}else{
    echo '增加管理员信息失败，错误信息：'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}


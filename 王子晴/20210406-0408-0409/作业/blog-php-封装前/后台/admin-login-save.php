<?php
/**
 *管理员登录保存页面
 */

//接收到管理员登录信息
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$isRemember = $_POST['isRemember'] ?? '';
$adminNum = $_POST['admin_randomNum'];

//判断验证码是否一致
session_start();

if(!($adminNum == $_SESSION['checkNum'])){
    echo "验证码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//查询该用户是否存在且信息正确
$sql = "select * from adminInfo where admin_email = '$adminEmail'";
$result = $db->query($sql);

//$errorInfo = $db->errorInfo()[2];
//print_r($errorInfo);

$adminInfo = $result->fetch(PDO::FETCH_ASSOC);

//验证密码是否与hash值匹配
$passwordResult = password_verify($adminPassword,$adminInfo['admin_password']);

if($adminInfo && $passwordResult === true){

    //登录成功后，设置相关信息
    if(!session_id()){
        session_start();
    }
    $_SESSION['admin_email'] = $adminEmail;
    $_SESSION['admin_name'] = $adminInfo['admin_name'];

    //点击了记住我，设置过期时间一天后
    if($isRemember){
       setcookie('PHPSESSID',session_id(),time()+10);
    }

    //记录登录成功日志
    $log = [
        'admin_email' => $_SESSION['admin_email'],
        'admin_name' => $_SESSION['admin_name'],
        'ip' => $_SERVER['REMOTE_ADDR'],        //ip地址
        'action' => 'login',
        'content' => '登录后台成功',
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLoginSuccess= json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLoginSuccess.PHP_EOL,FILE_APPEND);

    echo "登录成功<br/>";
    echo "<a href='article-list.php'>前往文章列表页</a>";
    exit();
}else{
    //记录登录失败日志
    if(empty($adminInfo)){
        $errorInfo = '数据库查找不到';
    }else if(empty($adminPassword)){
        $errorInfo = "密码为空";
    }else if($adminInfo['admin_password'] !== $adminPassword){
        $errorInfo = '密码错误';
    }
    $log = [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'action' => 'login',
        'content' => '登录后台失败',
        'errorInfo' => $errorInfo,
        'time' => date("Y-m-d H:i:s",time())
    ];
    $logLoginError = json_encode($log,JSON_UNESCAPED_UNICODE);
    file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logLoginError.PHP_EOL,FILE_APPEND);

    echo "姓名或密码错误，请重新输入，报错信息：".$db->errorInfo()[2]."<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
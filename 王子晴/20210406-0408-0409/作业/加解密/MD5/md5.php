<?php
/**
 * 常用的哈希算法、加密方法1——MD5
 */
//对123456进行MD5加密——用穷举法查找到明文密码：https://www.cmd5.com/
$str = "123456";
echo "一次MD5加密：".md5($str)."<br/><br/>";

//增加密码复杂度
//法一、多加密一次
echo "二次MD5加密：".md5(md5($str))."<br/>";
//法二、增加盐值(salt)——量子计算机
$salt = "x1?o/CARMK";
echo "使用工具增加盐值(salt)：".md5(md5($str.$salt).$salt)."<br/>";
//盐值只是一个随机的字符串，可以工具生成：http://tool.c7sky.com/password/，故还可以用验证码方法生成随机字符
$str2 = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+?<>~';
$max = mb_strlen($str2)-1;
$salt2 = '';
for($i=0;$i<10;$i++){
    $num = mt_rand(0,$max);
    $salt2 .= $str2[$num];
}
echo "使用验证码增加盐值（salt）：".md5(md5($str.$salt2).$salt2)."<br/><br/>";       //每次运行结果都不同

//对baidu.txt进行MD5加密
//读取文件
$file = file_get_contents("baidu.txt");
echo "对文本文档进行MD5加密：".md5($file);

<?php
/**
 * 对称加密
 */
//对 123456 进行一个简单的加密，加99：
$clearText = "123456";      //明文
$keys = 99;                 //密钥
echo "密文：".($clearText + $keys)."<br/>"; //密文

//对 123555 进行一个简单的解密，减99：
$cipherText = "123555";
echo "明文：".($cipherText - $keys)."<br/><br/>";  //明文

//封装（使用函数）
//对 123456 进行一个简单的加密，加99：
echo "密文：".encrypt("123456","99")."<br/>";
function encrypt($clearText,$keys){
    return $clearText + $keys;
}
//对 123555 进行一个简单的解密，减99：
echo "明文：".decode("123555","99")."<br/><br/>";
function decode($cipherText,$keys){
    return $cipherText - $keys;
}

//对 123456789 进行一个加密处理，每位数+1，如果结果为10，那么置为0：
echo "密文：".encrypt2("123456789","1")."<br/>";
function encrypt2($clearText,$keys){
    $len = strlen($clearText);
    $str = $clearText[$len];
    if($str == 10){
        $str == 0;
    }
}
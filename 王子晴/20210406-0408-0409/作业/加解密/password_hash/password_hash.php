<?php
/**
 * 加密方法2——password_hash
 */
//对123456进行password_hash()加密
$str = "123456";
$hash = password_hash($str,PASSWORD_BCRYPT);
echo "password_hash()加密：".$hash."<br/><br/>";      //每次运行结果都不同

//验证密码和hash值是否一致——password_verify()

var_dump(password_verify("1234567",$hash));      //false
echo "<br/>";
var_dump(password_verify("123456",$hash));       //true
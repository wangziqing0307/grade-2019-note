分类表
	分类id int 自增 主键
	分类名称 字符串（20）
	分类描述 字符串（200）
	增加时间 int 时间戳
	修改时间 int 时间戳

文章表
	文章id int 自增 主键
	文章标题 字符串（100）
	文章类别 int 不允许为空
	文章作者 字符串（20）
	文章内容 字符串（10000）
	增加时间 int 时间戳
	修改时间 int 时间戳
create database if not exists oa
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE utf8mb4_general_ci;
use oa;
-- php高级_01_mysql/05_mysql表的基本操作_创建表.md 第1,2题
-- 1. 创建员工信息表employee，表引擎为innodb，表字符集为utf8mb4。
create table employee(
		id int(11) unsigned auto_increment not null comment '员工编号',
		name varchar(25) not null comment '员工名称',
		deptId int(11) unsigned not null default '0' comment '所在部门编号',
		salary float(11,2) unsigned not null default '0' comment '工资',
		update_time int(11) unsigned not null comment '修改时间',
		add_time int(11) unsigned not null comment '增加时间',
		primary key (id)
)engine=innodb default charset=utf8mb4 comment='员工信息表';

-- 2. 创建部门信息表department，表引擎为innodb，表字符集为utf8mb4。
create table department(
		deptId int(11) UNSIGNED auto_increment not null comment '部门编号',
		name varchar(25) not null comment '部门名称',
		level int(11) UNSIGNED not null default '0' comment '部门等级',
		parentDeptId int(11) UNSIGNED not null default '0' comment '上级部门编号',
		deptLeader int(11) UNSIGNED not null default '0' comment '部门领导',
		update_time int(11) UNSIGNED not null comment '修改时间',
		add_time int(11) UNSIGNED not null comment '增加时间',
		primary key (deptId)
)engine=innodb default charset=utf8mb4 comment='部门信息表';

-- 2. 完成php高级_01_mysql/06_mysql表的基本操作_修改表.md 第1题
-- 1. 根据前面创建表，需改表名：
-- 	1. 修改 employee 表名为 employee_info。
alter table employee rename employee_info;

-- 3. 完成php高级_01_mysql/07_mysql表的基本操作_添加字段.md 第1题
-- 1. 员工信息表增加如下字段：
alter table employee_info add sex tinyint(3) UNSIGNED default '3' comment '员工性别：1男2女3保密';
alter table employee_info add join_time int(11) not null comment '入职时间';
alter table employee_info add address varchar(100) not null default ' ' comment '地址' after sex;

-- 4. 完成php高级_01_mysql/08_mysql表的基本操作_修改字段.md 第1题
-- 1. 员工信息表工资salary字段进行修改：
alter table employee_info change salary salary decimal(11,2) UNSIGNED not null default '0' comment '工资';
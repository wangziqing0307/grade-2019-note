<?php
/**
 * 20210510的练习
 */

namespace app\api\controller;


use think\facade\Request;
use think\facade\Validate;
class exercise2
{
    //1. 接第一章节练习题第2题，现在需要改进接口，移除首尾的空格再计算长度，请使用版本设计的方式改进。
    public function exercise1(){
        $v = Request::param('v',1);
        if($v == 1){        //版本为1时
            //2.设计一个api接口，可以计算出传入字符的长度，并返回json格式。
            //例如：传入 web api，返回结果：
            $str = Request::param('str');
            $validate = Validate::rule([
                'str|字符串' => 'require'
            ]);
            if(!$validate->check(['str'=>$str])){
                $data = [
                    'status' => 1,
                    'message' => '字符串不能为空',
                    'data' => []
                ];
                return json($data);
            }else{

                $data = [
                    'status' => 0,
                    'message' => '',
                    'data' => [
                        'length' => mb_strlen($str)
                    ]
                ];
                return json($data);
            }
        }else if ($v == 2){     //版本为2时
            //移除首尾的空格再计算长度
            $str = Request::param('str');
            $validate = Validate::rule([
                'str|字符串' => 'require'
            ]);
            if(!$validate->check(['str'=>$str])){
                $data = [
                    'status' => 1,
                    'message' => '字符串不能为空',
                    'data' => []
                ];
                return json($data);
            }else{
                $str = trim($str," ");      //移除首尾的空格
                $data = [
                    'status' => 0,
                    'message' => '',
                    'data' => [
                        'length' => mb_strlen($str)
                    ]
                ];
                return json($data);
            }
        }
    }
    //2. 接第一章节练习题第4题，现在需要改进接口，返回如下数据：
    public function exercise2(){
        $v = Request::param('v',1);
        if($v == 1){
            return $this->exercise2V1();
        }else if($v == 2){
            return $this->exercise2V2();
        }
    }
    public function exercise2V1(){
        //4. 设计一个api接口，可以分析一篇英文文章出现的单词列表，并返回json格式。格式如下：
        $content = Request::param('content');
        $validate = Validate::rule([
            'content|内容' => 'require'
        ]);
        if(!$validate->check(['content'=>$content])){
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }else{
            $arr = explode(" ",$content);   //php分隔字符串，返回数组
            $data = [
                'status' => 0,
                'message' => '',
                'data' => array_values(array_unique($arr))  //array_unique() —— 去掉重复元素 ； array_values() —— 下标重排
            ];
            return json($data);
        }
    }
    public function exercise2V2(){
        //分析一篇英文文章出现的单词列表，并记录单词出现总次数
        $content = Request::param('content');
        $validate = Validate::rule([
            'content|内容' => 'require'
        ]);
        if(!$validate->check(['content'=>$content])){
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }else{
            $arr = explode(" ",$content);   //php分隔字符串，返回数组
            $arrList = [];
            //法一（便于理解）
//                foreach ($arr as $word){
//                    $arrList[$word] = 0;
//                }
//                foreach ($arr as $word){
//                    $arrList[$word]++;
//                }
             // 法二
                foreach ($arr as $word){
                    if(empty($arrList[$word])){
                        $arrList[$word] = 0;
                    }
                    $arrList[$word]++;
                }
            $data = [
                'status' => 0,
                'message' => '',
                'data' => $arrList
            ];
            return json($data);
        }
    }
}
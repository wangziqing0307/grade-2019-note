<?php
/**
 * 服务端
 * 判断是否是斐波那契数
 */
if(mb_strlen($_POST['num']) == 0 || $_POST['num'] < 0){
    $data = [
        'status' => 1,
        'message' => '数字不能为空或需是正整数',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $arr = [0,1];
    //判断数字是否大于等于2
    if($_POST['num'] >= 2){
        $numLen = mb_strlen($_POST['num']);  //数字长度
        $unit = ($numLen/4)%10;  //个位数
        //算出要循环的次数
        if($numLen <= 3){     //数字长度:1~3
            $second = $numLen * 5;
        }else{  //数字长度:>=4
            $second = ($numLen-$unit)*5 + $unit*4;
        }
        //循环相应次数并将值存入元素
        for($i=2;$i<=$second+2;$i++){
//                  var_dump($i,$num);
            $arr[$i] = $arr[$i-1]+$arr[$i-2];       //斐波那契数列:该数的前一位加该数的后一位之和
//                  var_dump($arr);
        }
//        var_dump($arr);
    }

    //遍历数组——判断该元素在数组中是否存在
    foreach ($arr as $key=>$value){
        if($_POST['num'] == $value){
            $str = "该元素在数组中存在";
            $index = $key;

            $data = [
                'status' => 0,
                'message' => 'SUCCESS',
                'data' => [
                    'result' => $str,
                    'index' => $index
                ]
            ];
            header("Content-Type: application/json");
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
        }
    }
    //元素不存在
    $data = [
        'status' => 1,
        'message' => '该元素在数组中不存在',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);

}
<?php
/**
 * 客户端
 * 3. 设计表单页面，用户可以输入数字，然后使用ajax调用服务端斐波那契接口，判断是否是斐波那契数，并把结果显示在页面上。
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>使用ajax调用服务端斐波那契接口</title>
</head>
<body>
数字：<input type="number" name="num" id="number" value=""/>
<input type="submit" value="提交" id="btn"/>
<div id="content">

</div>
<script>
    document.getElementById("btn").onclick = function () {
        let num = document.getElementById("number").value;

        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                console.log(xmlHttp.responseText);
                let responseData = JSON.parse(xmlHttp.responseText);
                document.getElementById("content").innerHTML = "结果："+responseData.data.result+"<br/>下标："+responseData.data.index;
            }
        };
        xmlHttp.open("POST","/exercise20210511/exercise3(fbnNum)/SERVER.php",true);
        xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xmlHttp.send("num="+num);
    }

</script>
</body>

</html>
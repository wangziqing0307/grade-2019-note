<?php
/**
 * 客户端
 * 1. 设计表单页面，用户可以输入字符串，然后使用ajax调用服务端接口计算长度，并把结果显示在页面上。
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>使用ajax调用服务端接口计算长度</title>
</head>
<body>
    字符串：<input type="text" name="str" id="str" value=""/>
    <input type="submit" value="提交" id="btn"/>
    <div id="content">

    </div>
</body>
<script>
    document.getElementById("btn").onclick = function () {
        //获取post参数
        let str = document.getElementById("str").value;
        let xmlHttp = new XMLHttpRequest();     //异步请求
        xmlHttp.onreadystatechange = function () {
            // console.log("异步请求/xmlHttp请求的就绪状态",xmlHttp.readyState);
            // console.log("http响应状态",xmlHttp.status);
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                //法一（ajax）
                console.log(xmlHttp.responseText);  //服务端返回的内容
                //法二（json）
                let responseData = JSON.parse(xmlHttp.responseText);  //将服务端返回的内容字符串改成json对象
                document.getElementById("content").innerHTML = "字符串长度："+responseData.data.strLen;        //并把结果显示在页面上
            }
    };
    //开启请求
    xmlHttp.open("POST","/exercise20210511/exercise1(strLen)/server.php",true);
    //设置请求头
    xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    //发送请求
    xmlHttp.send("str="+str)
    }
</script>
</html>

<?php
/**
 * 服务端
 * 使用ajax调用服务端接口计算长度
 */
//echo"字符串：".$_POST['str'];


if(empty($_POST['str'])){
    $data = [
        'status' => '1',
        'message' => '字符串不能为空',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $data = [
        'status' => '0',
        'message' => 'SUCCESS',
        'data' => [
            'strLen' => mb_strlen($_POST['str'])
        ]
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}
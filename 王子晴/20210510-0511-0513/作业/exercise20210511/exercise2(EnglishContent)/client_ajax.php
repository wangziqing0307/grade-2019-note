<?php
/**
 * 客户端
 * 2. 设计表单页面，用户可以输入英文文章，然后使用ajax调用服务端接口统计词频，并把结果显示在页面上。
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>使用ajax调用服务端接口统计词频</title>
</head>
<body>
英语文章：<input type="text" name="content" id="EnglishContent" value=""/>
<input type="submit" value="提交" id="btn"/>
<div id="content">

</div>
<script>
    document.getElementById("btn").onclick = function () {
        let content = document.getElementById("EnglishContent").value;
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                console.log(xmlHttp.responseText);
                let responseData = JSON.parse(xmlHttp.responseText);
                document.getElementById("content").innerHTML = "单词列表："+responseData.data;
            }
        };
        xmlHttp.open("POST","/exercise20210511/exercise2(EnglishContent)/server.php",true);
        xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xmlHttp.send("content="+content);
    }
</script>
</body>

</html>
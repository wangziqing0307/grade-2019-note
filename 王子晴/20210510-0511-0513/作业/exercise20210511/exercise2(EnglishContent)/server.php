<?php
/**
 * 服务端
 * 分析一篇英文文章出现的单词列表
 */
if(empty($_POST['content'])){
    $data = [
        'status' => 1,
        'message' => '内容不能为空',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $arr = explode(" ",$_POST['content']);   //php分隔字符串，返回数组
    $data = [
        'status' => 0,
        'message' => 'SUCCESS',
        'data' => array_values(array_unique($arr))  //array_unique() —— 去掉重复元素 ； array_values() —— 下标重排
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}
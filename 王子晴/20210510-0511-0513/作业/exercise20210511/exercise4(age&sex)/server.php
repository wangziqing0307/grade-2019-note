<?php
/**
 * 服务端
 * 计算年龄和性别
 */
if(empty($_POST['num'])){
    $data = [
        'status' => 1,
        'message' => '身份证号不能为空',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else if(mb_strlen($_POST['num']) !== 18){
    $data = [
        'status' => 1,
        'message' => '身份证号不正确',
        'data' => []
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}else{
    $arr = array_map("intval",str_split($_POST['num']));
    $sex = $arr[17-1];
    $birthDay = '';
    for($i=6;$i<=9;$i++){
        $birthDay .=$arr[$i];
    }
    $thisYear = date("Y");      //当前年份
    $birthYear = $birthDay;             //生日年份
    $age = $thisYear - $birthYear;
    if($sex % 2 == 0){
        $chineseSex = "女";
    }else{
        $chineseSex = "男";
    }
    $data = [
        'status' => 0,
        'message' => 'SUCCESS',
        'data' => [
            'age' => $age,
            'sex' => $chineseSex
        ]
    ];
    header("Content-Type: application/json");
    echo json_encode($data,JSON_UNESCAPED_UNICODE);
}
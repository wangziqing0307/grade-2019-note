<?php
/**
 * 客户端
 * 4. 设计表单页面，用户可以输入身份证，然后使用ajax调用服务端身份证分析接口，计算年龄和性别，并把结果显示在页面上。
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>使用ajax调用服务端身份证分析接口</title>
</head>
<body>
身份证号：<input type="text" name="num" id="number" value=""/>
<input type="submit" value="提交" id="btn"/>
<div id="content">

</div>
<script>
    document.getElementById("btn").onclick = function () {
        let num = document.getElementById("number").value;

        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function () {
            if(xmlHttp.readyState == 4 && xmlHttp.status == 200){
                console.log(xmlHttp.responseText);
                let responseData = JSON.parse(xmlHttp.responseText);
                document.getElementById("content").innerHTML = "年龄："+responseData.data.age+"<br/>性别："+responseData.data.sex;
            }
        };
        xmlHttp.open("POST","/exercise20210511/exercise4(age&sex)/server.php",true);
        xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xmlHttp.send("num="+num);
    }
</script>
</body>

</html>
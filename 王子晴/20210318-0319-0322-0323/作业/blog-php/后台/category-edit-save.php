<?php
/**后台——编辑分类保存页面
 */
//获取到分类信息
$categoryId = $_POST['category_id'];
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条分类信息到数据库
$sql = "update category 
          set category_name = '$categoryName',category_desc = '$categoryDesc',update_time = '$updateTime'
          where category_id = '$categoryId'";
$result = $db->exec($sql);

//判断是否编辑分类信息成功
if($result){
    echo '编辑分类信息成功<br/><a href="category-list.php">返回分类列表页</a>';
    exit();
}else{
    echo '编辑分类信息失败，错误信息为'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}
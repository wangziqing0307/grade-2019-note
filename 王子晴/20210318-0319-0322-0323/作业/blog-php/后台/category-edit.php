<?php
/**
后台——分类编辑
 */
//获取到分类id
$categoryId = $_GET['category_id'];


//设置时区
date_default_timezone_set("PRC");

//连接到MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//查询一条分类信息
$sql = "select * from category where category_id = '$categoryId' ";
$result = $db->query($sql);
$category = $result->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——增加分类</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：admin &nbsp;<a href="#">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category-list.php">分类管理</a></li>
            <li><a href="article-list.php">文章管理</a></li>
            <li><a href="#">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article-list.php">首页</a>&gt;
                <a href="category-list.php">分类管理</a>&gt;
                <a href="category-list.php">分类列表</a>&gt;
                <a href="category-edit.php">编辑分类</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="category-edit-save.php" method="post">
                    <table>
                        <tr>
                            <td>分类id：</td>
                            <td><input type="text" name="category_id" value="<?php echo $category['category_id'] ?>" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name" value="<?php echo $category['category_name'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>分类描述：</td>
                            <td><textarea name="category_desc"><?php echo $category['category_desc'] ?></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<body>
</body>
</html>


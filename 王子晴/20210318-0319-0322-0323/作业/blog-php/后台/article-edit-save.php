<?php
/**后台——编辑文章保存页面
 */
//获取到文章信息
$articleId = $_POST['article_id'];
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$articleAuthor = $_POST['article_author'];
$articleContent = $_POST['article_content'];

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条文章信息到数据库
$sql = "update article 
          set article_title = '$articleTitle',category_id = '$categoryId',article_author = '$articleAuthor',article_content = '$articleContent',update_time = '$updateTime'
          where article_id = '$articleId'";
$result = $db->exec($sql);

//判断是否编辑文章信息成功
if($result){
    echo '编辑文章信息成功<br/><a href="article-list.php">返回文章列表页</a>';
    exit();
}else{
    echo '编辑文章信息失败，错误信息为'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}
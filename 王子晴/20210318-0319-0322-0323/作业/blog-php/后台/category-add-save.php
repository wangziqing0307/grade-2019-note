<?php
/**
 * 后台——增加分类保存页面
 */
//获取分类信息
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//增加一条分类信息到数据库
$sql = "insert into category (category_name,category_desc,add_time,update_time) 
          values ('$categoryName','$categoryDesc','$addTime','$updateTime')";
$result = $db->exec($sql);

//判断是否增加分类信息成功
if($result){
    echo '增加分类信息成功<br/><a href="category-list.php">返回分类列表页</a>';
    exit();
}else{
    echo '增加分类信息失败，错误信息：'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}

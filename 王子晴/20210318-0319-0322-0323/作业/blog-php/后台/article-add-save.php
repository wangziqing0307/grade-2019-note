<?php
/**
 * 后台——增加文章保存页面
 */
//获取文章信息
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$articleAuthor = $_POST['article_author'];
$articleContent = $_POST['article_content'];

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

//增加一条文章信息到数据库
$sql = "insert into article (article_title,category_id,article_author,article_content,add_time,update_time) 
          values ('$articleTitle','$categoryId','$articleAuthor','$articleContent','$addTime','$updateTime')";
$result = $db->exec($sql);

//判断是否增加分类信息成功
if($result){
    echo '增加文章信息成功<br/><a href="article-list.php">返回文章列表页</a>';
    exit();
}else{
    echo '增加文章信息失败，错误信息：'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}

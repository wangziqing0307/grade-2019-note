<?php
declare (strict_types = 1);

namespace app\api\controller;



use think\facade\Request;
use think\facade\Validate;

class Index
{
    public function isEvenNum()
    {
       $num = Request::param('num');
       $validate = Validate::rule([
           'num' => 'require|between:1,'.PHP_INT_MAX
       ]);
       if(!$validate -> check(['num'=>$num])){
           $data = [
               'status' => 1,               //返回的状态值：0表示正常；1表示参数为空
               'message' => $validate->getError(),      //返回的信息
               'data' => []
           ];
           echo json($data);   //把数组编码成json格式的数据
       }else{
           $data = [
               'status' => 0,
               'message' => '',
               'data' => [
                   'isEvenNum' => $num % 2 == 0
               ]
           ];
           echo json($data);
       }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/5/7
 * Time: 10:00
 */
namespace app\api\controller;
use think\facade\Request;
use think\facade\Validate;

class response{
    public function exercise6(){
        //设计一个api接口，传入一个整数，然后返回绝对值，格式是json格式
        $num = Request::param('num');
        $validate = Validate::rule([
           'num|数字' => 'require|between:'.PHP_INT_MIN.','.PHP_INT_MAX
        ]);
        if(!$validate->check(['num'=>$num])){
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => []
            ];
            //使用php原生json
            return json($data);
        }else if(strpos($num,".") !== false){
            $data = [
                'status' => 1,
                'message' => '数字必须是整数',
                'data' => []
            ];
            //使用php原生json
//            return json_encode($data,JSON_UNESCAPED_UNICODE);     //Content-Type: text/html; charset=utf-8

            //使用php原生json 将Content-Type改成 application/json; charset=utf-8
            header("Content-Type: application/json; charset=utf-8");
            echo json_encode($data,JSON_UNESCAPED_UNICODE);
            exit();
        }else{
            if($num < 0){
                $data = [
                    'status' => 0,
                    'message' => '',
                    'data' => [
                        'absoluteValue' => $num * (-1),
                    ]
                ];
            }else{
                $data = [
                    'status' => 0,
                    'message' => '',
                    'data' => [
                        'absoluteValue' => $num,
                    ]
                ];
            }
            return json($data);
        }
    }
}
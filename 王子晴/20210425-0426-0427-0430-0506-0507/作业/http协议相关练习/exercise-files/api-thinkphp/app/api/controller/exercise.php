<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/25
 * Time: 17:37
 */

namespace app\api\controller;


use think\facade\Request;
use think\facade\Validate;

class exercise
{
    public function exercise1(){
        //1. api和web api是什么？请说下你的理解，越详细越好。
			//api：是为程序员提供的一个接口，帮助实现某种功能，让程序员直接使用，不必纠结内部是如何实现的
			//Javascript语言由ECMAScript（JavaScriptyu语法）、BOM（浏览器对象模型）、DOM（页面文档对象模型）三部分组成
			//web api（包括BOM和DOM两部分）：主要针对浏览器提供的接口，主要针对于浏览器做交互效果
				     //一般都有输入和输出（函数的传参和返回值），很多都是方法（函数）
					 //学习web api可以结合学习内置对象方法的思路学习
    }
    public function exercise2(){
        //2.设计一个api接口，可以计算出传入字符的长度，并返回json格式。
        //例如：传入 web api，返回结果：
        $str = Request::param('str');
        $validate = Validate::rule([
            'str|字符串' => 'require'
        ]);
        if(!$validate->check(['str'=>$str])){
            $data = [
                'status' => 1,
                'message' => '字符串不能为空',
                'data' => []
            ];
            return json($data);
        }else{

            $data = [
                'status' => 0,
                'message' => '',
                'data' => [
                    'length' => mb_strlen($str)
                ]
            ];
            return json($data);
        }
    }
    public function exercise3(){
        //3. 设计一个api接口，可以计算出一个数字是否是斐波那契数列中的数字，并算出是第几位，并返回json格式。
        //斐波那契数列：1 1 2 3 5 8 13 21 ...
        //例如：传入 2，返回结果：
        $num = Request::param('num');
        $validate = Validate::rule([
            'num' => 'require'
        ]);
        if(!$validate->check(['num'=>$num])){
            $data = [
                'status' => 1,
                'message' => $validate->getError(),
                'data' => []
            ];
            return json($data);
        }else{
              $arr = [0,1];
              for($i=2;$i<=$num;$i++){
//                  var_dump($i,$num);
                  $arr[$i] = $arr[$i-1]+$arr[$i-2];
//                  var_dump($arr);
              }
              var_dump($arr);
            //判断该元素在数组中是否存在
            //array_key_exists($num,$arr);
//            array_key_exists(key,array)  布尔值
        }
    }
    public function exercise4(){
        //4. 设计一个api接口，可以分析一篇英文文章出现的单词列表，并返回json格式。格式如下：
    }
    public function exercise5(){
        // 5. 设计一个api接口，可以根据身份证号，计算出相应的年龄和性别，并返回json格式。
        //	1. 自行了解身份证格式。并写出你的理解，越详细越好。
            //1.十七位数字本体码和一位校验码组成，共十八位数字
            //2.从左往右：六位数字地址码，八位数字出生日期码，三位数字顺序码（其中第三位数字也就是第十七位，奇数为男，偶数为女）和一位数字校验码
            //2.1 六位数字地址码：1-2表示所在省份的代码，3-4表示所在城市的代码，5-6表示所在区县的代码
            //2.2 八位数字出生日期码：7-10表示年，11-12表示月，13-14表示日
            //2.3 三位数字顺序码:15-17表示同一地址辖区内的，以及同年同月同日生人的顺序码，同时第17位兼具性别标识功能，男单女双
            //2.4.尾号的校验码，由号码编制单位按统一的公式计算出来的，1到9或X（X是罗马数字的10）
        //	2. 根据身份证校验位，检测身份证是否符合格式，如果不符合，返回相关错误信息。
            $num = Request::param('num');
            $validate = Validate::rule([
                'num' => 'require|min:18|max:18'
            ]);
            if(!$validate->check(['num'=>$num])){
                $data = [
                    'status' => 1,
                    'message' => $validate->getError(),
                    'data' => []
                ];
                return json($data);
            }else{
                $arr = array_map("intval",str_split($num));
                $sex = $arr[17-1];
                $birthDay = '';
                for($i=6;$i<=9;$i++){
                    $birthDay .=$arr[$i];
                }
                $thisYear = date("Y");      //当前年份
                $birthYear = $birthDay;             //生日年份
                $age = $thisYear - $birthYear;

                $data = [
                    'status' => 0,
                    'message' => '',
                    'data' => [
                        'age' => $age,
                        'sex' => $sex % 2 == 0
                    ]
                ];
                return json($data);
            }
        //	3. 自行设计相关返回字段。


    }
    public function exercise6(){
        //6. 尝试将自己的api接口给其他同学调用，看是否能调用到。
    }
}
<?php
/**
 * 2.构造方法
 */
//示例1，声明一个类，其中包含姓名、性别、生日 这边三个属性，然后有一个构造方法：
class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;

    //2.构造方法
    public function __construct($name,$sex,$birthday)
    {
        $this->name = $name;
        $this->sex = $sex;
        $this->birthday = $birthday;
    }
}
//假如小明性别男，生日2001-10-05，实例化一个Human对象来表示小明：
$human1 = new Human("小明","男","2001-10-05");
var_dump($human1);
echo "<br/>";
//示例2，实例化一个Human对象，姓名为小红，性别女，生日2002-02-16。
$human2 = new Human("小红","女","2002-02-16");
var_dump($human2);
echo "<br/>";


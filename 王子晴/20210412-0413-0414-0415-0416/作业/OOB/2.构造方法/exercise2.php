<?php
/**
 * 构造方法的作业
 */
//1. 小军性别为男，生日为2001-08-26，请通过构造方法的形式，实例化Human对象。
class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;

    //构造方法
    public function __construct($name,$sex,$birthday)
    {
        $this->name = $name;
        $this->sex = $sex;
        $this->birthday = $birthday;
    }
}
$human = new Human("小军","男","2001-08-26");
var_dump($human);
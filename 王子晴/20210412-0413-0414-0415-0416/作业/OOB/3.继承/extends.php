<?php
/**
 * 3.继承
 */
////示例1，声明一个学生类，包含如下属性：姓名、性别、年龄、学校、院系、班级。
//class Student extends Human {
//    //定义属性
//    public $name;
//    public $sex;
//    public $birthday;
//    public $age;
//    public $college;
//    public $department;
//    public $class;
//
//    //构造方法
//    public function __construct($birthday)
//    {
//        $year = date("Y");
//        $birthdaySeconds = strtotime($birthday);
//        $birthYear = date("Y",$birthdaySeconds);
//
//        $age = $year - $birthYear;
//        $this->age = $age;
//    }
//
//}
////示例2，声明一个员工类，包含：工号、姓名、性别、出生年月、薪资
//class Worker extends Human {
//    //定义属性
//    public $workerId;
//    public $name;
//    public $sex;
//    public $birthday;
//    public $age;
//    public $money;
//
//    //构造方法
//    public function __construct($birthday)
//    {
//        $year = date("Y");
//        $birthdaySeconds = strtotime($birthday);
//        $birthYear = date("Y",$birthdaySeconds);
//
//        $age = $year - $birthYear;
//        $this->age = $age;
//    }
//
//}


//php继承实现（子类继承父类）
//示例1,声明Human类，学生类，继承Human类，具有如下属性：
class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;
    public $age;

    //构造方法
    public function __construct($birthday)
    {
        $year = date("Y");
        $birthdaySeconds = strtotime($birthday);
        $birthYear = date("Y",$birthdaySeconds);

        $age = $year - $birthYear;
        $this->age = $age;      //属性继承
    }
    public function eat(){      //方法继承
        echo "干饭";
    }
}
class Student extends Human {
    //定义属性
    public $college;
    public $department;
    public $class;
}
//示例2，员工类继承Human类:
class Worker extends Human {
    //定义属性
    public $workerId;
    public $money;
}
$worker1 = new Worker("2001-03-07");
var_dump($worker1->age);
echo "<br/>";

var_dump($worker1->eat());



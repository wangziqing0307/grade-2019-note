<?php
/**
 * 访问范围：public、protected和private关键词
 * public：都可访问
 * protected：同一个类和派生类都可访问，外部的类不可
 * private：同一个类可访问，派送类和外部的类不可
 */
//public、protected和private关键词
class Human{
    //定义属性
    public $name;
    protected $sex;
    private $birthday;

    public function __construct($sex)
    {
        $this->sex = $sex;
    }
    public function __construct2($birthday)
    {
        $this->birthday = $birthday;
    }
}
class Student extends Human {
    //定义属性
    public $college;
    public $department;
    public $class;

    //2.2、protected派生类（子类）调用$sex
    public function eat(){
        echo $this->sex."success";
    }

    //3.2、private派生类（子类）调用$birthday
    public function play(){
        echo $this->birthday();
    }
}
//示例2，员工类继承Human类:
class Worker extends Human {
    //定义属性
    public $workerId;
    public $money;
}


//2.3、protected外部的类调用$sex
//$human1 = new Human();
//$human1 -> sex = "男";
//var_dump($human1);      //报错

////2.2、protected派生类（子类）调用$sex
//$student2 = new Student("男");
//$student2->eat();     //输出“男success”


//3.3、private外部的类调用$birthday
//$human2 = new Human();
//$human2 -> birthday("2001-03-07");
//var_dump($human2);          //报错

//3.2、private派生类（子类）调用$birthday
//$human1->play();        //报错
<?php
/**
 * 1.类实例
 */
//类（class）
//示例1，声明Human的类，对人进行一个封装：
//class Human{
//
//}
//$human = new Human();
//var_dump($Human);

//属性（public）
//示例1，在Human类里声明 姓名、性别、生日 这边三个属性：
class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;

    //定义方法
    public function eat(){
        echo "干饭";
    }
    public function sleep(){
        echo "睡觉";
    }
    public function play(){
        echo "玩耍";
    }
    public function daily(){
        echo $this->name."学习";      //调用类中的属性
        $this -> eat();              //调用类中的方法
        $this -> sleep();
        $this -> play();
    }
}
$human = new Human();
var_dump($human);
echo "<br/>";
//实例（instance）
//示例1，假如小明性别男，生日2001-10-05，实例化一个Human对象来表示小明：
$human1 = new Human();
$human1 -> name = "小明";
$human1 -> sex = "男";
$human1 -> birthday = "2001-10-05";
var_dump($human1);
echo "<br/>";
//示例2，假如小红性别女，生日2002-02-16，实例化小红这个对象：
$human2 = new Human();
$human2 -> name = '小红';
$human2 -> sex = '女';
$human2 -> birthday = '2002-02-16';
var_dump($human2);
echo "<br/>";
//示例3，假如小军性别男，生日2001-08-26，实例化小军这个对象：
$human3 = new Human();
$human3 -> name = "小军";
$human3 -> sex = '男';
$human3 -> birthday = '2001-08-26';
var_dump($human3);
echo "<br/>";

//方法（method）
//示例1，在上面声明的类Human里声明一个eat()方法，打印出干饭：
$human -> eat();
echo "<br/>";
$human1 -> eat();
echo "<br/>";
//继续声明睡觉、玩耍的方法：
$human1 -> play();
echo "<br/>";
$human1 -> sleep();     //睡觉
sleep(1);       //休眠函数，休眠一秒
echo "<br/>";

//$this关键词
$human -> daily();

//调用属性

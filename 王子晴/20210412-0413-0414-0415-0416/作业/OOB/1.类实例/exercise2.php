<?php
/**
 * 类实例的作业
 */
//1. 声明一个Human类，包含了姓名、性别、生日属性，定义一个方法计算出年龄。
class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;
    //定义方法
    public function age(){
        $year = date("Y");      //当前日期（年）

        $birthDateSeconds = strtotime($this->birthday);     //生日日期的时间戳
        $birthYear = date("Y",$birthDateSeconds);   //生日（年）

        return $this->name.($year - $birthYear);      //当前年龄

    }
}
$human = new Human();
$human -> name = "王月馨";
$human -> sex = "女";
$human -> birthday = "2000-02-19";
var_dump($human);
echo "<br/>";
print_r($human->age());
echo "<br/>";

//2. 声明一个汽车Car类，包含了品牌、车型、价格等等属性，属性可以自由定义，可以参考汽车之家等相关平台。
class Car{
    //定义属性
    public $brand;      //品牌
    public $carType;    //车型
    public $price;      //价格
}
$car1 = new Car();
$car1 -> brand = "奥迪";
$car1 -> carType = "A3";
$car1 -> price = "193200";
var_dump($car1);

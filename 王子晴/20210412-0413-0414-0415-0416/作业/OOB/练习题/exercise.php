<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 16:47
 */
//1. 声明一个Human类，包含了姓名、性别、生日属性，定义一个方法计算出年龄。
class Human {
    //定义属性
    public $name;
    public $sex;
    public $age;

    //构造函数
//    public function __construct($birthday)
//    {
//        $year = date("Y");
//
//        $birthDateSeconds = strtotime($birthday);
//        $birthYear = date("Y",$birthDateSeconds);
//
//        $age = $year - $birthYear;
//
//        $this->age = $age;
//    }

    public function __construct($name,$sex,$birthday)
    {
        $year = date("Y");

        $birthDateSeconds = strtotime($birthday);
        $birthYear = date("Y",$birthDateSeconds);

        $age = $year - $birthYear;

        $this->name = $name;
        $this->sex = $sex;
        $this->age = $age;
    }


}
//	1. 实例化Human对象，并设置相关姓名、性别和生日。
//$human1 = new Human("2000-02-19");
//$human1 -> name = "王月馨";
//$human1 -> sex = "女";
//var_dump($human1);
//	2. 实例化Human对象，通过构造方法初始化相关姓名、性别和生日。
$human2 = new Human("全贵兰","女","2000-01-02");
var_dump($human2);
//2. 现在要做一个汽车网站，
//	1. 声明一个汽车Car类，包含了品牌、车型、价格等等属性，属性可以自由定义，可以参考汽车之家等相关平台。
class Car{
    //定义属性
    public $brand;      //品牌
    public $carType;    //车型
    public $price;      //价格
}
//	2. 声明新能源车类继承Car类，并包含新能源车相关属性。
class newEnergy extends Car {
    //定义属性

}
//	3. 声明汽油车类继承Car类，并包含汽油车相关属性。
class gasoline extends Car{
    //定义属性
}
//3. 使用面向对象的思想 分析 https://leetcode-cn.com/ 网站，设计相关的类，并设计相关属性。比如：题目、企业、用户等等。
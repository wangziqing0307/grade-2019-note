<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/13
 * Time: 16:28
 */
class  Worker{
    public $workerId;
    public $money;
}
$worker = new Worker();
var_dump($worker);
echo "<br/>";

include_once "Bar/Worker.php";
$workerBar = new \Bar\Worker();
var_dump($workerBar);
echo "<br/>";

include_once "Foo/Worker.php";
$workerFoo = new \Foo\Worker();
var_dump($workerFoo);
echo "<br/>";

include_once "Bar/Temp/Worker.php";
$workerBarTemp = new Bar\Temp\Worker();
var_dump($workerBarTemp);
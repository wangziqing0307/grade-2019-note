<?php
/**
 * 5.抽象类
 * 一般用在父类，然后父类不能被实例化
 * 抽象类一般只应用在不想被实例化的场景。
 */
//Human类封装了共同的属性和方法，在业务中只是使用员工类，Human这个类不希望被实例化。

abstract class Human{
    //定义属性
    public $name;
    public $sex;
    public $birthday;
}

class Worker extends Human {
    //定义属性
    public $workerId;
    public $money;
}

//$human = new Human();       //会报错，无法实例化

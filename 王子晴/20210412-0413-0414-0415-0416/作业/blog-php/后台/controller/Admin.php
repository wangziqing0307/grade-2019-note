<?php
/**
 * 管理员
 */
namespace controller;
include_once APP_PATH."./controller/Controller.php";
include_once APP_PATH."./model/Admin.php";

class Admin extends Controller {
    public function adminList(){
          $this->checkLogin();

        $adminModel = new \model\Admin();
        $adminInfoList = $adminModel -> getAdminList();

        $this -> display("admin-list",["adminInfoList"=>$adminInfoList]);
    }

    public function add(){
         $this->checkLogin();

        $this -> display("admin-add",[]);
    }

    public function add_save(){
        //接收增加管理员信息
        $adminName = $_POST['admin_name'];
        $adminEmail = $_POST['admin_email'];
        $adminPassword = $_POST['admin_password'];
        $againPassword = $_POST['again_password'];

        //邮箱5~100长度
        $this -> validate($adminEmail,5,100,"邮箱长度5~100");
        //姓名2~30长度
        $this -> validate($adminName,2,30,"姓名长度2~30");
        //密码6~20长度
        $this -> validate($adminPassword,6,20,"密码长度6~20");
        $this -> validate($againPassword,6,20,"密码长度6~20");

        //判断密码是否一致
        $this -> equality($adminPassword,$againPassword,"密码不一致，请重新输入");
        //对密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $adminPassword = md5($salt.md5($salt.$adminPassword.$salt).$salt);

        $adminModel = new \model\Admin();
        $result = $adminModel -> adminAddSave($adminName,$adminEmail,$adminPassword);

        $this -> display("admin-add-save",["result"=>$result]);
    }

    public function edit(){
         $this->checkLogin();

        //接收管理员id
        $adminId = $_GET['admin_id'];

        $adminModel = new \model\Admin();
        $adminInfo = $adminModel -> adminEdit($adminId);

        $this -> display("admin-edit",["adminInfo"=>$adminInfo]);
    }

    public function edit_save(){
        //接收编辑管理员信息
        $adminEmail = $_POST['admin_email'];
        $adminName = $_POST['admin_name'];
        $adminPassword = $_POST['admin_password'];
        $againPassword = $_POST['again_password'];

        //姓名2~30长度
        $this -> validate($adminName,2,30,"姓名长度2~30");
        //确认密码6~20长度
        $this -> validate($againPassword,6,20,"密码长度6~20");
        if(mb_strlen($adminPassword) !== 32){
            //密码6~20长度
            $this -> validate($adminPassword,6,20,"密码长度6~20");
            //对密码进行md5加密
            $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
            $adminPassword = md5($salt.md5($salt.$adminPassword.$salt).$salt);

        }
        //对确认密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $againPassword = md5($salt.md5($salt.$againPassword.$salt).$salt);



        //判断密码是否一致
        $this -> equality($adminPassword,$againPassword,"密码不一致，请重新输入");

        $adminModel = new \model\Admin();
        $result = $adminModel -> adminEditSave($adminEmail,$adminName,$adminPassword);

        $this -> display("admin-edit-save",["result"=>$result]);
    }

    public function login(){
        $this -> display("admin-login",[]);
    }
    public function login_save(){
        //接收到管理员登录信息
        $adminEmail = $_POST['admin_email'];
        $adminPassword = $_POST['admin_password'];
        $isRemember = $_POST['isRemember'] ?? '';
        $adminNum = $_POST['admin_randomNum'];

        //判断验证码是否一致
        session_start();

        $this -> equality($adminNum,$_SESSION['checkNum'],"验证码不一致，请重新输入");

        $adminModel = new \model\Admin();
        $arr = $adminModel -> adminLoginSave($adminEmail,$adminPassword,$isRemember);
        $adminInfo = $arr[0];
        $adminPassword = $arr[1];
        $db = $arr[2];

        $this -> display("admin-login-save",["adminInfo"=>$adminInfo,"adminPassword"=>$adminPassword,"db"=>$db]);
    }
    public function logout(){
        //开启会话
        session_start();

        $adminModel = new \model\Admin();
        $adminModel -> adminLogout();

        $this -> display("admin-logout",[]);
    }
}
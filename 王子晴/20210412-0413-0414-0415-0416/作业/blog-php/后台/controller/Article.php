<?php
/**
 * 文章
 */
namespace controller;
include_once APP_PATH."./controller/Controller.php";
include_once APP_PATH."./model/Article.php";

class Article extends Controller {
    public function articleList(){
          $this->checkLogin();

        $articleModel = new \model\Article();
        $arr = $articleModel->getArticleList();
        $articleList = $arr[0];
        $db = $arr[1];

        $this -> display("article-list",["articleList"=>$articleList,"db"=>$db]);
    }

    public function add(){
        $this->checkLogin();

        $articleModel = new \model\Article();
        $categoryList = $articleModel -> articleAdd();

        $this -> display("article-add",["categoryList"=>$categoryList]);
    }

    public function add_save(){
        //获取文章信息
        $articleTitle = $_POST['article_title'];
        $categoryId = $_POST['category_id'];
        $articleAuthor = $_POST['article_author'];
        $articleContent = $_POST['article_content'];

        //表单验证(后台管理功能细节)
        //文章标题5到50个字
        $this -> validate($articleTitle,5,50,"文章标题5-50个字");
        //文章分类必须选择
        if($categoryId === "0"){        //要么==="0" 要么==0
            echo "需要选择文章分类<br/>";
            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
            exit();
        }
        //文章作者必须填写
        $this -> validate($articleAuthor,1,PHP_INT_MAX,"文章作者不能为空");
        //文章简介必须填写，10-100个字
        //文章内容必须填写，10-8000个字
        $this -> validate($articleContent,10,8000,"文章内容10~8000个字");

        $articleModel = new \model\Article();
        $arr = $articleModel -> articleAddSave($articleTitle,$categoryId,$articleAuthor,$articleContent);
        $result = $arr[0];
        $db = $arr[1];

        $this -> display("article-add-save",["result"=>$result,"db"=>$db]);
    }

    public function edit(){
          $this->checkLogin();

        //获取到文章id
        $articleId = $_GET['article_id'];

        $articleModel = new \model\Article();
        $arr = $articleModel -> articleEdit($articleId);

        $categoryList = $arr[0];
        $article = $arr[1];

        $this -> display("article-edit",["categoryList"=>$categoryList,"article"=>$article]);
    }
    public function edit_save(){
        //获取到文章信息
        $articleId = $_POST['article_id'];
        $articleTitle = $_POST['article_title'];
        $categoryId = $_POST['category_id'];
        $articleAuthor = $_POST['article_author'];
        $articleContent = $_POST['article_content'];

        //表单验证(后台管理功能细节)
        //文章id不能为空
        $this -> validate($articleId,1,PHP_INT_MAX,"文章id不能为空");
        //文章标题5到50个字
        $this -> validate($articleTitle,5,50,"文章标题5-50个字");
        //文章作者必须填写
        $this -> validate($articleAuthor,1,PHP_INT_MAX,"文章作者不能为空");
        //文章内容必须填写，10-8000个字
        $this -> validate($articleContent,10,8000,"文章内容10~8000个字");

        $articleModel = new \model\Article();
        $result = $articleModel -> articleEditSave($articleId,$articleTitle,$categoryId,$articleAuthor,$articleContent);

        $this -> display("article-edit-save",["result"=>$result]);
    }

    public function delete(){
        //获取文章id
        $articleId = $_GET['article_id'];

        $articleModel = new \model\Article();
        $result = $articleModel -> articleDelete($articleId);

        $this -> display("article-delete",["result"=>$result]);
    }
    public function delete_multi(){
        //获取一个或多个文章id
        $articleIds = $_POST['article'];

        $articleModel = new \model\Article();
        $result = $articleModel -> articleDeleteMulti($articleIds);

        $this -> display("article-delete-multi",["result"=>$result]);
    }
}
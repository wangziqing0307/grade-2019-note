<?php
/**
 * 检查是否登录
 */
namespace controller;
class Controller{
    //检查是否登录
    public function checkLogin(){
        //开启会话
        session_start();

        if(empty($_SESSION['admin_email'])){
            echo "尚未登录，<br/>请<a href='index.php?c=Admin&a=login'>前往登录页面</a>,重新登录";
            exit();
        }
    }
    //判断列名长度
    public function validate($field,$min,$max,$message){
        if(mb_strlen($field)<$min || mb_strlen($field)>$max){
            echo "$message<br/>";
            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
            exit();
        }
    }
    //判断是否值是否相等
    public function equality($field1,$field2,$message1){
        if(!($field1 == $field2)){
            echo "$message1<br/>";
            echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
            exit();
        }
    }
    //封装view
    //渲染模板，包含视图文件
    public function display($templatePath,$templateVars){

        extract($templateVars);
        include_once APP_PATH."./view/{$templatePath}.php";
    }
}



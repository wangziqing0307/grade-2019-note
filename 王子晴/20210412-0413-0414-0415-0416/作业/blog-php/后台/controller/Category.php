<?php
/**
 * 分类
 */
namespace controller;

include_once APP_PATH."./controller/Controller.php";
include_once APP_PATH."./model/Category.php";

class Category extends Controller {
    public function categoryList(){

        $this->checkLogin();

        $categoryModel = new \model\Category();
        $categoryList = $categoryModel->getCategoryList();

        //引入视图
        $this -> display("category-list",["categoryList"=>$categoryList]);
    }
    public function add(){
         $this->checkLogin();

         $this -> display("category-add",[]);
    }

    public function add_save(){
        //获取分类信息
        $categoryName = $_POST['category_name'];
        $categoryDesc = $_POST['category_desc'];

        //表单验证(后台管理功能细节)
        //分类名称2~45个字
        $this -> validate($categoryName,2,45,"分类名称2~45个字符");
        //分类说明10~255个字
        $this -> validate($categoryDesc,10,255,"分类说明10~255个字符");

        $categoryModel = new \model\Category();
        $result = $categoryModel->CategoryAddSave($categoryName,$categoryDesc);

        $this -> display("category-add-save",["result"=>$result]);
    }

    public function edit(){
         $this->checkLogin();

        //获取到分类id
        $categoryId = $_GET['category_id'];

        $categoryModel = new \model\Category();
        $category = $categoryModel -> categoryEdit($categoryId);

        $this -> display("category-edit",["category"=>$category]);
    }

    public function edit_save(){
        //获取到分类信息
        $categoryId = $_POST['category_id'];
        $categoryName = $_POST['category_name'];
        $categoryDesc = $_POST['category_desc'];

        //表单验证(后台管理功能细节)
        //分类id不能为空
        $this -> validate($categoryId,1,PHP_INT_MAX,"分类id不能为空");
        //分类名称2~45个字
        $this -> validate($categoryName,2,45,"分类名称2~45个字符");
        //分类说明10~255个字
        $this -> validate($categoryDesc,10,255,"分类说明10~255个字符");

        $categoryModel = new \model\Category();
        $result = $categoryModel-> categoryEditSave($categoryId,$categoryName,$categoryDesc);

        $this -> display("category-edit-save",["result"=>$result]);
    }
    public function delete(){
        //获取分类id
        $categoryId = $_GET['category_id'];

        //表单验证(后台管理功能细节)
        //分类id不能为空
        $this -> validate($categoryId,1,PHP_INT_MAX,"分类id不能为空");

        $categoryModel = new \model\Category();
        $result = $categoryModel -> categoryDelete($categoryId);

        $this -> display("category-delete",["result"=>$result]);
    }
}
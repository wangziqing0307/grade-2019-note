<?php
/**
 * 生成验证码图片
 */
//生成图片：148 * 50
$image = imagecreate(148,50);
//背景色：白色
$bgColor = imagecolorallocate($image,255,255,255);
//字体颜色：红51，绿108，蓝175
$fontColor = imagecolorallocate($image,51,108,175);
//声明一个空变量，用于存放验证码
$machineRandomNum = '';

include_once APP_PATH."./model/randomNum.php";
include_once APP_PATH."./view/randomNum.php";
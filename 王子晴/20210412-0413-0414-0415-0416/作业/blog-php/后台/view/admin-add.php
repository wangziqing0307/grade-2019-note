<?php
/**
 *后台——增加管理员
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Admin&a=adminList">管理员管理</a>&gt;
                <a href="index.php?c=Admin&a=adminList">管理员列表</a>&gt;
                <a href="index.php?c=Admin&a=add">增加管理员</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="index.php?c=Admin&a=add_save" method="post">
                    <table>
                        <tr>
                            <td>邮箱：</td>
                            <td><input type="text" name="admin_email"/></td>
                        </tr>
                        <tr>
                            <td>姓名：</td>
                            <td><input type="text" name="admin_name"/></td>
                        </tr>
                        <tr>
                            <td>密码：</td>
                            <td><input type="password" name="admin_password"/></td>
                        </tr>
                        <tr>
                            <td>确认密码：</td>
                            <td><input type="password" name="again_password"/></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include_once APP_PATH."./view/public/footer.php"; ?>

<?php
/**
后台——文章编辑
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Article&a=articleList">文章管理</a>&gt;
                <a href="index.php?c=Article&a=articleList">文章列表</a>&gt;
                <a href="index.php?c=Article&a=edit">编辑文章</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="index.php?c=Article&a=edit_save" method="post">
                    <table>
                        <input type="hidden" name="article_id" value="<?php echo $article['article_id'] ?>"/>
                        <tr>
                            <td>文章标题：</td>
                            <td><input type="text" name="article_title" value="<?php echo $article['article_title'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>文章分类：</td>
                            <td>
                                <select name="category_id">
                                    <?php foreach ($categoryList as $item): ?>
                                        <option value="<?php echo $item['category_id'] ?>" <?php echo $article['category_id']==$item['category_id'] ? 'selected="selected"': ''; ?>>
                                            <?php echo $item['category_name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>文章作者：</td>
                            <td><input type="text" name="article_author" value="<?php echo $article['article_author'] ?>" /></td>
                        </tr>
                        <tr>
                            <td>文章内容：</td>
                            <td><textarea name="article_content"><?php echo $article['article_content'] ?></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include_once APP_PATH."./view/public/footer.php"; ?>


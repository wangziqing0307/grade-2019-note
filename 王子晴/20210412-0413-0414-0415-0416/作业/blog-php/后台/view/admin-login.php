<?php
/**
 *管理员登录页面
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>非常日记后台管理——登录</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
</head>
<body>
<div>
    <form action="index.php?c=Admin&a=login_save" method="post">
        <p align="center" class="front_white">非常日记后台管理</p>
        <table align="center">
            <tr>
                <td id="table_title" align="center">请输入邮箱和密码</td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header" >邮箱<input type="text" name="admin_email" class="input_box"/></li></td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header" >密码<input type="password" name="admin_password" class="input_box" /></li></td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header" id="underline2">验证码<input type="text" name="admin_randomNum" class="input_box" id="random_box" /><img src="index.php?c=randomNum" id="random_img"/></li></td>
            </tr>
            <tr>
                <td class="table_content"><input type="checkbox" name="isRemember" /><b id="remember">记住我</b></td>
            </tr>
            <tr>
                <td><input type="submit" value="登录" class="front_white" id="table_login" /></td>
            </tr>
        </table>
        <p id="last_li">
            <a href="../admin-register.php" class="front_white" style="font-size:14px;">新员工&nbsp;?&nbsp;&nbsp;创建账号</a>
        <p/>
    </form>
</div>
</body>
</html>


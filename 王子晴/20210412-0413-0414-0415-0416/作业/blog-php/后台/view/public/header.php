<?php
/**
 * 页面的头部和左部
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——文章列表</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
    <script src="js/jquery.js"></script>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="index.php?c=Admin&a=logout">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="index.php?c=Category&a=categoryList">分类管理</a></li>
            <li><a href="index.php?c=Article&a=articleList">文章管理</a></li>
            <li><a href="index.php?c=Admin&a=adminList">管理员</a></li>
        </ul>
    </div>
<?php
/**
后台——分类增加
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类管理</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类列表</a>&gt;
                <a href="index.php?c=Category&a=add">增加分类</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="index.php?c=Category&a=add_save" method="post">
                    <table>
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name"/></td>
                        </tr>
                        <tr>
                            <td>分类描述：</td>
                            <td><textarea name="category_desc"></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

<?php include_once APP_PATH."./view/public/footer.php"?>

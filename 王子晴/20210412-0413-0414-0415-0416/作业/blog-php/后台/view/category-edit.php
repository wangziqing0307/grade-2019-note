<?php
/**
后台——分类编辑
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类管理</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类列表</a>&gt;
                <a href="index.php?c=Category&a=edit">编辑分类</a>
            </div>
            <div class="table-list" id="table-add" >
                <form action="index.php?c=Category&a=edit_save" method="post">
                    <table>
                        <tr>
                            <td>分类id：</td>
                            <td><input type="text" name="category_id" value="<?php echo $category['category_id'] ?>" readonly="readonly"/></td>
                        </tr>
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name" value="<?php echo $category['category_name'] ?>"/></td>
                        </tr>
                        <tr>
                            <td>分类描述：</td>
                            <td><textarea name="category_desc"><?php echo $category['category_desc'] ?></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置"  class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>

<?php include_once APP_PATH."./view/public/footer.php"?>
<?php
/**
后台——分类列表
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类管理</a>&gt;
                <a href="index.php?c=Category&a=categoryList">分类列表</a>
            </div>
            <div id="table-menu">
<!--                <button class="btn">全选</button>-->
<!--                <a href="#">删除选中任务</a>-->
                <a href="index.php?c=Category&a=add" id="add_article">增加分类</a>
            </div>
            <div class="table-list" id="list">
                <form action="#" method="post">
                    <table>
                        <tr>
                            <th id="fist_th"></th>
                            <th>分类id</th>
                            <th>分类名称</th>
                            <th>分类描述</th>
                            <th>增加时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach ($categoryList as $row): ?>
                        <tr>
                            <td><input type="checkbox" id="table_checkbox" /></td>
                            <td><?php echo $row['category_id'] ?></td>
                            <td><?php echo $row['category_name'] ?></td>
                            <td><?php echo $row['category_desc'] ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['add_time']) ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$row['update_time']) ?></td>
                            <td>
                                <a href="index.php?c=Category&a=edit&category_id=<?php echo $row['category_id'] ?>">编辑</a>
                                <a href="index.php?c=Category&a=delete&category_id=<?php echo $row['category_id'] ?>">删除</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php include_once APP_PATH."./view/public/footer.php"; ?>
<?php
/**
后台——文章列表
 */
include_once APP_PATH."./view/public/header.php";
?>

    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="index.php?c=Article&a=articleList">首页</a>&gt;
                <a href="index.php?c=Article&a=articleList">文章管理</a>&gt;
                <a href="index.php?c=Article&a=articleList">文章列表</a>
            </div>
            <div id="table-menu">
                <button class="btn" id="all">全选</button>
                <a id="article-multi-delete" href="javascript:void(0)">删除选中任务</a>
                <a href="index.php?c=Article&a=add" id="add_article">增加文章</a>
            </div>
            <div class="table-list" id="list">
                <form id="article-form" action="index.php?c=Article&a=delete_multi" method="post">
                    <table>
                        <tr>
                            <th id="fist_th"></th>
                            <th>文章id</th>
                            <th>文章标题</th>
                            <th>文章分类</th>
                            <th>文章作者</th>
                            <th>发表时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach ($articleList as $item): ?>
                            <tr>
                                <td><input type="checkbox" id="table_checkbox" class="article_checkbox" name="article[]" value="<?php echo $item['article_id'] ?>" /></td>
                                <td><?php echo $item['article_id'] ?></td>
                                <td><?php echo $item['article_title'] ?></td>
                                <td><?php
                                    $sql = "select * from category where category_id='{$item['category_id']}'";
                                    $result = $db -> query($sql);
                                    $category = $result -> fetch(PDO::FETCH_ASSOC);
                                    echo $category['category_name'];
                                    ?></td>
                                <td><?php echo $item['article_author'] ?></td>
                                <td><?php echo date("Y-m-d H:i:s",$item['add_time']) ?></td>
                                <td><?php echo date("Y-m-d H:i:s",$item['update_time']) ?></td>
                                <td>
                                    <a href="index.php?c=Article&a=edit&article_id=<?php echo $item['article_id'] ?>">编辑</a>
                                    <a id="delete" href="index.php?c=Article&a=delete&article_id=<?php echo $item['article_id'] ?>">删除</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </form>
            </div>
        </div>
    </div>
<script src="js/main.js"></script>

<?php include_once APP_PATH."./view/public/footer.php"?>

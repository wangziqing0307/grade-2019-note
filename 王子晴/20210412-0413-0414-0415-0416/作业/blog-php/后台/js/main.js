//全选
$("#all").click(function () {
    $(".article_checkbox").prop("checked",true);
});
//批量删除
$("#article-multi-delete").click(function () {
    if(confirm("是否确认批量删除？")){
        $("#article-form").submit();
    }
});
//删除
$("#delete").click(function () {
    if(confirm("是否确认删除")){
        return true
    }
    return false
});
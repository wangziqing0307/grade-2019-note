<?php
/**
 *管理员注册页面
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>非常日记后台管理——注册</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
</head>

<body>
<div id="register_div">
    <p align="center" class="front_white">非常日记后台注册页面</p>
    <form action="admin-register-save.php" method="post">
        <table>
            <tr>
                <td id="table_title" align="center">请输入相关信息</td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header">姓名<input type="text" name="admin_name" class="input_box" /></li></td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header">密码<input type="password" name="admin_password" class="input_box" /></li></td>
            </tr>
            <tr>
                <td class="table_content"><li class="table_header">确认密码<input type="password" name="again_password" class="input_box" /></li></td>
            </tr>
            <tr>
                <td><input type="submit" value="注册" name="register" id="table_register" class="front_white" /></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>


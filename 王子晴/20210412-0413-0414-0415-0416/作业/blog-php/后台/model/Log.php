<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/15
 * Time: 22:06
 */
namespace model;
include_once APP_PATH."./model/Model.php";

class Log extends Model{
    public function write($log){
        $logBase = [
            'ip' => $_SERVER['REMOTE_ADDR'],        //ip地址
            'time' => date("Y-m-d H:i:s",time())
        ];
        //将两个数组合成一个
        $log = array_merge($log,$logBase);

        $logCategoryAddSuccess = json_encode($log,JSON_UNESCAPED_UNICODE);
        file_put_contents('logs/'.date("Y-m-d",time()).'.txt',$logCategoryAddSuccess.PHP_EOL,FILE_APPEND);
    }
}
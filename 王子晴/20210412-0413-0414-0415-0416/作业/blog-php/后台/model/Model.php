<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/14
 * Time: 14:50
 */
namespace model;

class Model{
    public function contentDB(){
        //连接MySQL数据库
        $dsn = "mysql:host=localhost;dbname=blog";
        $db = new \PDO($dsn, "root", "123456");
        $db -> exec("set names utf8mb4");
        return $db;
    }
    public function queryAll($sql){

        $db = $this->contentDB();
        $result = $db->query($sql);
        //var_dump($db->errorInfo());
        //exit();
        $List = $result->fetchAll(\PDO::FETCH_ASSOC);
        return $List;
    }
    public function queryOne($sql){
        $db = $this->contentDB();
        $result = $db->query($sql);
        $One = $result->fetch(\PDO::FETCH_ASSOC);
        return $One;
    }
    public function exec($sql){
        $db = $this->contentDB();
        $result = $db->exec($sql);
        return $result;
    }

}
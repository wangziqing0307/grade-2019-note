<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/14
 * Time: 21:30
 */
namespace model;
include_once APP_PATH."./model/Model.php";
include_once APP_PATH."./model/Log.php";

class Article extends Model {
    public function getArticleList(){
        $db = $this->contentDB();

        //查询表article
        $sql = "select * from article order by article_id asc ";
        $List = $this -> queryAll($sql);
        $arr = [$List,$db];
        return $arr;
    }
    public function articleAdd(){
        $sql = "select * from category";
        $List = $this -> queryAll($sql);
        return $List;
    }
    public function articleAddSave($articleTitle,$categoryId,$articleAuthor,$articleContent){



        $db = $this -> contentDB();

        $addTime = time();
        $updateTime = $addTime;

        //增加一条文章信息到数据库
        $sql = "insert into article (article_title,category_id,article_author,article_content,add_time,update_time) 
          values ('$articleTitle','$categoryId','$articleAuthor','$articleContent','$addTime','$updateTime')";
        $result = $this->exec($sql);

        if($result){
            //增加文章成功日志
            $log = new Log();
            $log -> write([
                'article_title' => $articleTitle,
                'category_id' => $categoryId,
                'article_author' => $articleAuthor,
                'article_content' => $articleContent,
                'action' => 'add',
                'content' => '增加文章成功'
            ]);

        }
        $arr = [$result,$db];
        return $arr;
    }
    public function articleEdit($articleId){
        //查询分类表
        $sql = "select * from category";
        $List = $this -> queryAll($sql);

        //查询一条文章信息
        $sql = "select * from article where article_id = '$articleId' ";
        $One = $this -> queryOne($sql);

        $arr = [$List,$One];
        return $arr;
    }
    public function articleEditSave($articleId,$articleTitle,$categoryId,$articleAuthor,$articleContent){
        $updateTime = time();
        //修改一条文章信息到数据库
        $sql = "update article 
          set article_title = '$articleTitle',category_id = '$categoryId',article_author = '$articleAuthor',article_content = '$articleContent',update_time = '$updateTime'
          where article_id = '$articleId'";
        $result = $this->exec($sql);

        if($result){
            //编辑文章成功日志
           $log = new Log();
           $log -> write([
               'article_id' => $articleId,
               'article_title' => $articleTitle,
               'category_id' => $categoryId,
               'article_author' => $articleAuthor,
               'article_content' => $articleContent,
               'action' => 'edit',
               'content' => '编辑文章成功'
           ]);

        }
        return $result;
    }
    public function articleDelete($articleId){

        $addTime = time();
        $updateTime = $addTime;

        //删除一条文章信息到数据库
        $sql = "delete from article where article_id = '$articleId'";
        $result = $this->exec($sql);

        return $result;
    }
    public function articleDeleteMulti($articleIds){

        $addTime = time();
        $updateTime = $addTime;

        //删除一条或多条文章信息到数据库
        $sql = "delete from article where article_id in (".implode(",",$articleIds).")";
        $result = $this->exec($sql);

        return $result;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/14
 * Time: 23:23
 */
namespace model;
include_once APP_PATH."./model/Model.php";
include_once APP_PATH."./model/Log.php";

class Admin extends Model {

    public function getAdminList(){
        //查询表adminInfo
        $sql = "select * from adminInfo order by admin_id asc ";
        $List = $this -> queryAll($sql);
        return $List;
    }

    public function adminAddSave($adminName,$adminEmail,$adminPassword){
        $addTime = time();
        $updateTime = $addTime;


        //插入一条管理员注册信息
        $sql = "insert into adminInfo (admin_name,admin_email,admin_password,add_time,update_time) 
          values ('$adminName','$adminEmail','$adminPassword','$addTime',$updateTime)";
        $result = $this->exec($sql);

        return $result;
    }
    public function adminEdit($adminId){
        //查询一条管理员信息
        $sql = "select * from adminInfo where admin_id = '$adminId' ";
        $One = $this -> queryOne($sql);

        return $One;
    }
    public function adminEditSave($adminEmail,$adminName,$adminPassword){

        $updateTime = time();

        //修改一条管理员信息到数据库
        $sql = "update adminInfo 
          set admin_name = '$adminName',admin_password = '$adminPassword',update_time = '$updateTime'
          where admin_email = '$adminEmail'";
        $result = $this->exec($sql);

        return $result;
    }
    public function adminLoginSave($adminEmail,$adminPassword,$isRemember){

        $db = $this ->contentDB();

        //查询该用户是否存在且信息正确
        $sql = "select * from adminInfo where admin_email = '$adminEmail'";
        $adminInfo = $this -> queryOne($sql);

        //将用户登录的密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $adminPassword = md5($salt.md5($salt.$adminPassword.$salt).$salt);
//        echo $adminPassword;exit();  //3a6faaae642982ac56d9827c22cf29a0

        if($adminInfo && $adminPassword === $adminInfo['admin_password']){
            //登录成功后，设置相关信息
            if(!session_id()){
                session_start();
            }
            $_SESSION['admin_email'] = $adminEmail;
            $_SESSION['admin_name'] = $adminInfo['admin_name'];

            //点击了记住我，设置过期时间一天后
            if($isRemember){
                setcookie('PHPSESSID',session_id(),time()+10);
            }

            //记录登录成功日志
          $log = new Log();
          $log -> write([
              'admin_email' => $_SESSION['admin_email'],
              'admin_name' => $_SESSION['admin_name'],
              'action' => 'login',
              'content' => '登录后台成功'
          ]);


        }else{
            //记录登录失败日志
            if(empty($adminInfo)){
                $errorInfo = '数据库查找不到';
            }else if(empty($adminPassword)){
                $errorInfo = "密码为空";
            }else if($adminInfo['admin_password'] !== $adminPassword){
                $errorInfo = '密码错误';
            }
            $log = new Log();
            $log -> write([
                'action' => 'login',
                'content' => '登录后台失败',
                'errorInfo' => $errorInfo
            ]);
        }

        $arr = [$adminInfo,$adminPassword,$db];
        return $arr;
    }
    public function adminLogout(){
        //删除session
        unset($_SESSION['admin_email']);
        unset($_SESSION['admin_name']);

        //记录登出日志
        if(empty($_SESSION['admin_email'])){
            $log = new Log();
            $log -> write([
                'admin_email' => null,
                'admin_name' => null,
                'action' => 'logout',
                'content' => '登出后台成功'
            ]);
        }
    }
}
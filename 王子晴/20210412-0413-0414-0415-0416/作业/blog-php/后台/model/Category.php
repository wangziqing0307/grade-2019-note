<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/14
 * Time: 16:27
 */
namespace model;

include_once APP_PATH."./model/Model.php";
include_once APP_PATH."./model/Log.php";

class Category extends Model{

    public function getCategoryList(){
        $sql = "select * from category order by category_id asc ";
        $List = $this -> queryAll($sql);
        return $List;
    }

    public function CategoryAddSave($categoryName,$categoryDesc){
        $addTime = time();
        $updateTime = $addTime;
        //增加一条分类信息到数据库
        $sql = "insert into category (category_name,category_desc,add_time,update_time) 
          values ('$categoryName','$categoryDesc','$addTime','$updateTime')";
        $result = $this -> exec($sql);

        if($result){
            //增加分类成功日志
            $log = new Log();
            $log -> write([
                'category_name' => $categoryName,
                'category_desc' => $categoryDesc,
                'action' => 'add',
                'content' => '增加分类成功'
            ]);
        }
        return $result;
    }

    public function categoryEdit($categoryId){
        //查询一条分类信息
        $sql = "select * from category where category_id = '$categoryId' ";
        $One = $this ->queryOne($sql);
        return $One;
    }

    public function categoryEditSave($categoryId,$categoryName,$categoryDesc){
        $updateTime = time();

        //修改一条分类信息到数据库
        $sql = "update category 
          set category_name = '$categoryName',category_desc = '$categoryDesc',update_time = '$updateTime'
          where category_id = '$categoryId'";
        $result = $this->exec($sql);

        if($result){
            //编辑分类成功日志
           $log = new Log();
           $log -> write([
               'category_id' => $categoryId,
               'category_name' => $categoryName,
               'category_desc' => $categoryDesc,
               'action' => 'edit',
               'content' => '编辑分类成功'
           ]);

        }
        return $result;
    }

    public function categoryDelete($categoryId){

        //表单验证(后台管理功能细节)
        //点击删除，如果分类下有文章，那么提示：分类下有文章，请先删除相关文章
        $articleSql = "select * from article where category_id = '$categoryId' limit 1";

        $articleList = $this ->queryAll($articleSql);

        if(!empty($articleList)){
            echo "分类下有文章，请先删除相关文章<br/>";
            echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
            exit();
        }

        $addTime = time();
        $updateTime = $addTime;

        //删除一条分类信息到数据库
        $sql = "delete from category where category_id = '$categoryId'";
        $result = $this->exec($sql);

        return $result;
    }
}
<?php
/**
 * 引入文件
 */
//入口文件
//echo dirname(__FILE__);     //C:\Users\WZQ\Desktop\IT\nginx-1.18.0\html\blog-php\后台

define("APP_PATH",dirname(__FILE__));

$controller = $_GET['c'] ?? 'Admin';
if (empty($controller)) {
    echo '页面没有找到';
    exit();
}

//从controller目录下引入php文件：分类、文章、管理员
include_once APP_PATH."./controller/{$controller}.php";

$controller = "\controller\\".$controller;
if(class_exists($controller)){          //类名是否存在
    //调用方法：列表，增加，增加保存，编辑，编辑保存，删除
    $controllerAction = new $controller();

    $action = $_GET['a'] ?? 'login';
    if(empty($action)){
        echo "页面没有找到";
        exit();
    }

    $controllerAction -> $action();
}


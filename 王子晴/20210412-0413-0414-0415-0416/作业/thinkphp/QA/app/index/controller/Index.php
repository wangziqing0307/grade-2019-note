<?php
declare (strict_types = 1);

namespace app\index\controller;

class Index
{
    public function index()
    {
        return '您好！这是前台应用：用来给大家访问的应用，大家可以在上面提问和回答问题。';
    }
}

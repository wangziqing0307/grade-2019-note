-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: jing_dong
-- ------------------------------------------------------
-- Server version   5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `goods`
--

DROP TABLE IF EXISTS `goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `cate_id` int(10) unsigned NOT NULL,
  `brand_id` int(10) unsigned NOT NULL,
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `is_show` tinyint(3) NOT NULL DEFAULT '1',
  `is_saleoff` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `cate_id` (`cate_id`),
  KEY `brand_id` (`brand_id`),
  CONSTRAINT `goods_ibfk_1` FOREIGN KEY (`cate_id`) REFERENCES `goods_cates` (`id`),
  CONSTRAINT `goods_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `goods_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods`
--

--
-- Table structure for table `goods_brands`
--

DROP TABLE IF EXISTS `goods_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_brands`
--
--
-- Table structure for table `goods_cates`
--

DROP TABLE IF EXISTS `goods_cates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `goods_cates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `goods_cates`
--

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-23 14:54:00
/*品牌数据*/
insert into goods_brands(id,name) values (1,'华硕'),
	(2,'唯爱'),
	(3,'戴尔'),
	(4,'爱戴尔'),
	(5,'索尼'),
	(6,'联想'),
	(7,'苹果'),
	(8,'雷蛇'),
	(16,'海尔'),
	(17,'清华同方'),
	(18,'神舟');

/*分类数据*/
insert into goods_cates(id,name) values (1,'台式机'),
	(2,'平板电脑'),
	(3,'电脑配件'),
	(4,'笔记本'),
	(5,'超极本'),
	(6,'超级本'),
	(8,'路由器'),
	(9,'交换机'),
	(10,'网卡');

insert into goods(id,name,cate_id,brand_id,price,is_show,is_saleoff) values (1,'r510vc 15.6英寸笔记本',4,1,3399,'1','0'),
	(2,'x550cc 15.6英寸笔记本',4,1,2799,'1','0'),
	(3,'x240 超极本',5,6,4880,'1','0'),
	(4,'u330p 13.3英寸超级本',5,6,4299,'1','0'),
	(5,'svp13226scb 触控超级本',6,5,7999,'1','0'),
	(6,'ipad mini 7.9英寸平板电脑',2,7,1999,'1','0'),
	(7,'iPad air 9.7英寸平板电脑',2,7,3388,'1','0'),
	(8,'iPad mini 配置 retine 显示屏',2,7,2788,'1','0'),
	(9,'ideacentre c3340 20英寸一体电脑',1,6,3499,'1','0'),
	(10,'vostro 3800-r1206 台式电脑',1,3,2899,'1','0'),
	(11,'15.6 寸电脑屏保护膜',3,4,29,'1','0'),
	(12,'优雅 复古 无线鼠标键盘',3,8,299,'1','0'),
	(13,'15寸 4K 液晶显示屏',3,5,1899,'1','0'),
	(14,'限量款 LOL 鼠标垫',3,2,29,'1','0')
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/20
 * Time: 11:24
 */
namespace app\model;
//定位商品表
class GoodsModel extends \think\Model {
    protected $name = "goods";  //对应到数据库的表名
    protected $pk = "id";       //表的主键名字，primary key
}

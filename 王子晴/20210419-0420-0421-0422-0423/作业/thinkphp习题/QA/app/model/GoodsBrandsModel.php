<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/20
 * Time: 11:32
 */
namespace app\model;
use think\Model;
//定位品牌表
class GoodsBrandsModel extends Model{
    protected $name = "goods_brands";
    protected $pk = "id";
}
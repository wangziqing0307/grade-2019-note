<?php
declare (strict_types = 1);

namespace app\admin\controller;

class Index
{
    public function index()
    {
        return '您好！这是后台应用：用来管理大家的问题，屏蔽一些不合法的发言等等。';
    }
}

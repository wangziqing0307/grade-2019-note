<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/16
 * Time: 15:13
 */
declare (strict_types=1);

namespace app\index\controller;

use think\facade\Request;
use think\facade\Validate;

class Question
{
    public function detail()
    {
        //6. 已知QA系统中，问题详情方法中（代码参考题5），需要获取到问题id（question_id）参数，并查询相应的问题信息。
            //2. 请问thinkphp框架如何获取question_id这个参数。
                $question_id = Request::param('question_id');
                var_dump($question_id);     //字符串
                echo "<br/>";

            //4. 对参数quesiton_id进行验证，question_id必须，并且是正整数。
                $params = Request::param();
                var_dump($params);  //数组
                echo "<br/>";
                $validate = Validate::rule([
                    'question_id|问题id' => 'require|between:1,'.PHP_INT_MAX
                ]);
                var_dump($validate->check($params));    //check需要数组，不然报错
                echo "<br/>";
                //法一
//                if(!($validate->check($params))){
//                    echo $validate->getError()."检测没有通过";
//                }else{
//                    echo "检测通过<br/>";
//                }
                //法二
                if(($validate->check($params)) === true){
                    echo "检测通过<br/>";
                }else{
                    echo $validate->getError()."<br/>检测没有通过";
                }
        return '问题详情页面';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/20
 * Time: 11:24
 */
namespace app\index\controller;
use app\model\GoodsBrandsModel;
use app\model\GoodsCatesModel;
use app\model\GoodsModel;

class goods{
    public function exercise(){
// 8. 请参考 81_电商数据库.sql，将其导入到mysql数据库，使用thinkphp模型做如下练习：
    //	1. 参考 82_电商数据.md，系统增加了品牌、分类和商品，将其中相关数据插入到对应表。
        //品牌表中插入数据
        //$GoodsBrands是一个模型对象，里面存储了数据库对应的字段信息
//        $GoodsBrands = GoodsBrandsModel::create(['id'=>'1','name'=>'华硕']);
//        var_dump($GoodsBrands);
//        echo "<br/>";
//        echo $GoodsBrands->id;      //或$GoodsBrands['id'];
    //	2. r510vc 15.6英寸笔记本 已经售罄，请修改相关数据。
//        $Goods = GoodsModel::update(['is_saleoff'=>'1'],['name'=>'r510vc 15.6英寸笔记本']);
//        if($Goods){
//            echo "修改成功";
//            exit();
//        }else{
//            echo "修改失败";
//            exit();
//        }
    //	3. 15.6 寸电脑屏保护膜 下架处理，请修改相关数据。
//        $Goods = GoodsModel::update(['is_show'=>'0'],['name'=>'15.6 寸电脑屏保护膜']);
//        if($Goods){
//            echo "修改成功";
//            exit();
//        }else{
//            echo "修改失败";
//            exit();
//        }
    //	4. 查询所有的商品、品牌和分类数据。
        //查询商品表
        $goods = GoodsModel::select();
        echo "商品表：<pre>{$goods}</pre>";
        echo "<br/>";
        //查询品牌表
        $goodsBrand = GoodsBrandsModel::select();
        echo "品牌表：<pre>{$goods}</pre>";
        echo "<br/>";
        //查询分类表
        $goodsCates = GoodsCatesModel::select();
        echo "分类表：<pre>{$goodsCates}</pre>";
        echo "<br/>";
    //	5. 查询所有的商品，并且按照价格降序排列。
        //select * from goods order by price desc;
        $goodsDesc = GoodsModel::order('price','desc')->select();
        echo "商品表，降序：<pre>{$goodsDesc}</pre>";
        echo "<br/>";
    //	6. 查询出品牌是戴尔的商品。
        //查询品牌是戴尔
        $DaleBrand = GoodsBrandsModel::where('name','=','戴尔')->find();
//        echo "<pre>{$DaleBrand["id"]}</pre>";
        //查询品牌是戴尔的商品
        $DaleGoods = GoodsModel::where('brand_id','=',$DaleBrand["id"])->select();
        echo "查询品牌是戴尔的商品：<pre>{$DaleGoods}</pre>";
        echo "<br/>";
    //	7. 查询出所有超极本的商品。
        //查询分类是超极本
        $superCate = GoodsCatesModel::getByName('超极本');
        echo "<pre>{$superCate}</pre>";
        //查询出所有超极本的商品。
        $superGoods = GoodsModel::where('cate_id','=',$superCate['id'])->select();
        echo "查询出所有超极本的商品:<pre>{$superGoods}</pre>";
    //	8. 查询华硕品牌的超极本。
        //查询品牌是华硕
        $AsusBrand = GoodsBrandsModel::where('name','=','华硕')->find();
//        echo "<pre>{$AsusBrand['id']}</pre>";
        //查询分类是超极本
        $superCate = GoodsCatesModel::where('name','=','超极本')->find();
//        echo "<pre>{$superCate['id']}</pre>";
        //查询华硕品牌的超极本
        $AsusSuperGoods = GoodsModel::where('brand_id','=',$AsusBrand['id'])->where('cate_id','=',$superCate['id'])->select();
        echo "查询华硕品牌的超极本:<pre>{$AsusSuperGoods}</pre>";
    //	9. 查询出价格最高的商品和最低的商品。
        //查询出价格最高的商品
        $priceMaxGoods = GoodsModel::where('price','=',GoodsModel::max('price'))->find();
        echo "查询出价格最高的商品:<pre>{$priceMaxGoods}</pre>";
        //查询出价格最低的商品
        $priceMinGoods = GoodsModel::where('price','=',GoodsModel::min('price'))->find();
        echo "查询出价格最高的商品:<pre>{$priceMinGoods}</pre>";
    //	10. 查询是否有售罄的商品。
        //查询有售罄的商品
        $saleoffGoods = GoodsModel::where('is_saleoff','=','1')->select();
        echo "查询有售罄的商品:<pre>{$saleoffGoods}</pre>";
        if($saleoffGoods){
            echo "有售罄的商品";
        }else{
            echo "无售罄的商品";
        }
        echo "<br/>";
    //	11. 查询所有商品、品牌和分类总数。
        //查询所有商品的总数
        $goodsCount = GoodsModel::count('id');
        echo "查询所有商品的总数:<pre>{$goodsCount}</pre>";
        //查询所有品牌的总数
        $BrandsCount = GoodsBrandsModel::count('id');
        echo "查询所有品牌的总数:<pre>{$BrandsCount}</pre>";
        //查询所有分类的总数
        $catesCount = GoodsCatesModel::count('id');
        echo "查询所有分类的总数:<pre>{$catesCount}</pre>";
    //	12. 每页显示10件商品，查询出第1页和第2页的数据。
        $firstGoodsTen = GoodsModel::limit(0,10)->select();
        echo "查询商品表第一页数据，10件商品:<pre>{$firstGoodsTen}</pre>";
        $secondGoodsTen = GoodsModel::limit(10,10)->select();
        echo "查询商品表第二页数据，10件商品:<pre>{$secondGoodsTen}</pre>";

    }
    //模型数据操作
    public function exercise2(){
        //一、查询数据
        //查询多条，品牌表id为1，2，3，4
        $GoodBrandList = GoodsBrandsModel::select([1,2,3,4]);
       foreach ($GoodBrandList as $value){
           echo "<pre>{$value['id']}</pre>";
           echo "<pre>{$value -> name}</pre>";
       }
       //动态查询
       $GoodCateList = GoodsCatesModel::getbyId('1');
       echo $GoodCateList -> name;

        // 获取某个商品的价格
        $priceId2 = GoodsModel::where('id','2')->value('price');
        echo "<pre>{$priceId2}</pre>";
        // 获取某个列的所有值
        $nameCateId3 = GoodsModel::where('cate_id',3)->column('name');
        var_dump($nameCateId3);
        echo $nameCateId3[0];
        echo "<br/>";
        // 以id为索引
        $nameCateId1 = GoodsModel::where('cate_id',1)->column('name','id');
        var_dump($nameCateId1);
        echo $nameCateId1[9];       //此时数组下标为id
        echo "<br/>";

        //聚合查询
        //如果你的字段不是数字类型，是使用max/min的时候，需要加上第二个参数。
        $max = GoodsBrandsModel::max('name',false);
        var_dump($max);

        //二、更新数据
            //查找并更新
               $cateId1 = GoodsCatesModel::find(1); // 查找出1的分类
               $cateId1['name'] = '000'; // 设置名称
               $cateId1->save();// 保存到数据库  //返回布尔值
            //直接更新
               GoodsCatesModel::update(['name' => '台式机'], ['id' => 1]);
            //批量更新
        //三、删除数据
            //删除当前模型 delete
            //根据主键删除 destroy
            //条件删除 where delete
            //sql直接处理 \think\facade\Db::query(sql查询语句)
    }
}
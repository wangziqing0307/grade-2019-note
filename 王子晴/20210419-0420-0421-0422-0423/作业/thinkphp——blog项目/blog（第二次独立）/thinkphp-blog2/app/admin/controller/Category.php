<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 8:25
 */

namespace app\admin\controller;
use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Category
{
    //分类列表
    public function index(){
        parse_str(Request::query(),$queryParams);
        $categoryList = CategoryModel::paginate([
            'list_rows' => 2,
            'query' => $queryParams
        ]);
        View::assign('categoryList',$categoryList);
        return View::fetch();
    }
    //分类增加或编辑
    public function addOrEdit(){
        $categoryId = Request::param('category_id');
        if(!$categoryId){  //增加
            return View::fetch('add');
        }else{              //编辑
            $validate = Validate::rule([
                'category_id|分类id' => 'require|between:1,'.PHP_INT_MAX
            ]);
            if(!$validate->check(['category_id'=>$categoryId])){
                $errors = $validate->getError();
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            $categoryOne = CategoryModel::where('category_id',$categoryId)->find();
            if(!$categoryOne){
                $errors = "该分类不存在，请重新选择";
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            return View::fetch('edit',['categoryOne' => $categoryOne]);
        }
    }
    //分类增加保存
    public function addSave(){
        $params = Request::param();
        $validate = Validate::rule([
           'category_name|分类标题' => 'require|min:2|max:45',
            'category_desc|分类简介' => 'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $params['add_time'] = time();
        $params['update_time'] = $params['add_time'];
        $result = CategoryModel::create($params);

        return View::fetch('public/tips',['result'=>$result,'name' => 'category','str' =>'请返回分类列表页面']);

    }

    //分类编辑保存
    public function editSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,'.PHP_INT_MAX,
            'category_name|分类标题' => 'require|min:2|max:45',
            'category_desc|分类简介' => 'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $categoryOne = CategoryModel::where('category_id',$params['category_id'])->find();
        $categoryOne['category_name'] = $params['category_name'];
        $categoryOne['category_desc'] = $params['category_desc'];
        $categoryOne['update_time'] = time();
        $result = $categoryOne->save();

        return View::fetch('public/tips',['result'=>$result,'name' => 'category','str' =>'请返回分类列表页面']);
    }
    //分类删除
    public function delete(){
        $categoryId = Request::param('category_id');
        $validate = Validate::rule([
            'category_id|分类id' => 'require|between:1,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['category_id'=>$categoryId])){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        //判断该分类下是否有管理员
        $articleOne = ArticleModel::where('category_id',$categoryId)->find();
        if($articleOne){
            $errors = "该分类下有管理员,不能删除";
            return View::fetch('public/errors',['errors'=>$errors]);
        }

        $result = CategoryModel::destroy($categoryId);

        return View::fetch('public/tips',['result'=>$result,'name' => 'category','str' =>'请返回分类列表页面']);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 12:18
 */

namespace app\admin\controller;


use app\model\AdminModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Login
{
    //登录页面
    public function index(){
        return View::fetch();
    }
    //登录保存
    public function loginSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'admin_email|邮箱' => 'require|min:5|max:100',
            'admin_password|密码' => 'require|min:6|max:20',
            'verify_code|验证码' => 'require|min:6|max:6'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        session_start();
        if($params['verify_code'] !== $_SESSION['checkNum']){
            $errors = "验证码不一致，请重新输入";
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);
        $result = AdminModel::where('admin_email',$params['admin_email'])->find();
        if($result && $params['admin_password'] == $result['admin_password']){
            session('admin_email',$params['admin_email']);
            session('admin_name',$result['admin_name']);

            if(!empty($params['isRemember'])){
                setcookie('PHPSESSID',session_id(),time()+86400);
            }
            return View::fetch('public/tips',['result'=>$result,'name' => 'article','str' =>'请前往文章列表页面']);
        }

    }
    //登出页面
    public function logout(){
        session('admin_email',null);
        session('admin_name',null);
        $result = empty(session('admin_email'));
        return View::fetch('public/tips',['result'=>$result,'name' => 'login','str' =>'请前往登录页面']);
    }
    //验证码
    public function verifyCode(){
        $image = imagecreate(148,60);
        $imageBgcolor = imagecolorallocate($image,255,255,255);
        $imageFontcolor = imagecolorallocate($image,51,108,75);
        $machineRandomNum = '';
        for($i=1;$i<=6;$i++){
            $num = mt_rand(0,9);
            imagestring($image,6,20*$i,20,$num,$imageFontcolor);
            $machineRandomNum .= $num;
        }
        session_start();
        $_SESSION['checkNum'] = $machineRandomNum;
        imageline($image,12,28,135,28,$imageFontcolor);
        header("Content-Type:image/png");
        imagepng($image);
        imagedestroy($image);
        exit();

    }
}
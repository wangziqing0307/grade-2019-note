<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 12:56
 */
namespace app\admin\middleware;
class Auth
{
    public function handle($request,\Closure $next){

        if(!preg_match('/login/',$request->pathinfo())){
            if(!session('admin_email')){
                return redirect("/index.php?s=admin/login/index");
            }
        }
        $response = $next($request);
        return $response;
    }
}
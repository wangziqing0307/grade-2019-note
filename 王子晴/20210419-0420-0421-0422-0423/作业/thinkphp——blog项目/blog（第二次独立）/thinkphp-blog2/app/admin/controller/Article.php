<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 10:00
 */

namespace app\admin\controller;


use app\model\ArticleModel;
use app\model\CategoryModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Article
{
    //管理员列表
    public function index(){
        $articleList = ArticleModel::getList();
        View::assign('articleList',$articleList);
        return View::fetch();
    }
    //管理员增加或编辑
    public function addOrEdit(){
        $articleId = Request::param('article_id');
        if(!$articleId){        //增加
            $categoryList = CategoryModel::select();
            View::assign('categoryList',$categoryList);
            return View::fetch('add');
        }else{                  //编辑
            $validate = Validate::rule([
                'article_id|管理员id' => 'require|between:1,'.PHP_INT_MAX
            ]);
            if(!$validate->check(['article_id'=>$articleId])){
                $errors = $validate->getError();
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            $articleOne = ArticleModel::where('article_id',$articleId)->find();
            if(!$articleOne){
                $errors = "该管理员不存在，请重新选择";
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            $categoryList = CategoryModel::select();
            return View::fetch('edit',['articleOne'=>$articleOne,'categoryList'=>$categoryList]);
        }
    }
    //管理员增加保存
    public function addSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'article_title|管理员标题' => 'require|min:5|max:50',
            'category_id|管理员分类' => 'require|between:1,'.PHP_INT_MAX,
            'intro|管理员简介' => 'require|min:10|max:100',
            'content|管理员内容' => 'require|min:10|max:8000'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $params['add_time'] = time();
        $params['update_time'] = $params['add_time'];
        $result = ArticleModel::create($params);
        return View::fetch('public/tips',['result'=>$result,'name' => 'article','str' =>'请返回文章列表页面']);
    }

    //管理员编辑保存
    public function editSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'article_id|管理员id' => 'require|between:1,'.PHP_INT_MAX,
            'article_title|管理员标题' => 'require|min:5|max:50',
            'category_id|管理员分类' => 'require|between:1,'.PHP_INT_MAX,
            'intro|管理员简介' => 'require|min:10|max:100',
            'content|管理员内容' => 'require|min:10|max:8000'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $articleOne = ArticleModel::where('article_id',$params['article_id'])->find();
        $articleOne['article_title'] = $params['article_title'];
        $articleOne['category_id'] = $params['category_id'];
        $articleOne['intro'] = $params['intro'];
        $articleOne['content'] = $params['content'];
        $articleOne['update_time'] = time();
        $result = $articleOne->save();
        return View::fetch('public/tips',['result'=>$result,'name' => 'article','str' =>'请返回文章列表页面']);
    }
    //管理员删除
    public function delete(){
        $articleId = Request::param('article_id');
        $validate = Validate::rule([
            'article_id|管理员id' => 'require|between:1,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['article_id'=>$articleId])){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $result = ArticleModel::destroy($articleId);
        return View::fetch('public/tips',['result'=>$result,'name' => 'article','str' =>'请返回文章列表页面']);
    }
}
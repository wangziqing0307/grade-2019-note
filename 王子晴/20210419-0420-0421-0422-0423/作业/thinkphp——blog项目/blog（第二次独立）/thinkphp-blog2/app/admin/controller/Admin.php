<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 11:25
 */

namespace app\admin\controller;


use app\model\AdminModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Admin
{
    //管理员列表
    public function index(){
        parse_str(Request::query(),$queryParams);
        $adminList = AdminModel::paginate([
            'list_rows' => 2,
            'query' => $queryParams
        ]);
        View::assign('adminList',$adminList);
        return View::fetch();
    }
    //管理员增加或编辑
    public function addOrEdit(){
        $adminId = Request::param('admin_id');
        if(!$adminId){      //增加
            return View::fetch('add');
        }else{              //编辑
            $validate = Validate::rule([
                'admin_id|管理员id' => 'require|between:1,'.PHP_INT_MAX
            ]);
            if(!$validate->check(['admin_id'=>$adminId])){
                $errors = $validate->getError();
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            $adminOne = AdminModel::where('admin_id',$adminId)->find();
            if(!$adminOne){
                $errors = "该用户不存在，请重新选择";
                return View::fetch('public/errors',['errors'=>$errors]);
            }
            View::assign('adminOne',$adminOne);
            return View::fetch('edit');
        }
    }
    //管理员增加保存
    public function addSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'admin_name|用户名' => 'require|min:2|max:30',
            'admin_email|邮箱' => 'require|min:5|max:100',
            'admin_password|密码' => 'require|min:6|max:20',
            'admin_password_again|密码' => 'require|min:6|max:20'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        if($params['admin_password'] !== $params['admin_password_again']){
            $errors = "密码不一致，请重新输入";
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);
        $params['add_time'] = time();
        $params['update_time'] = $params['add_time'];
        $result = AdminModel::create($params);
        return View::fetch('public/tips',['result'=>$result,'name' => 'admin','str' =>'请返回管理员列表页面']);
    }

    //管理员编辑保存
    public function editSave(){
        $params = Request::param();
        $validate = Validate::rule([
            'admin_id|管理员id' => 'require|between:1,'.PHP_INT_MAX,
            'admin_name|用户名' => 'require|min:2|max:30',
            'admin_email|邮箱' => 'require|min:5|max:100',
            'admin_password|密码' => 'require|min:6|max:20',
            'admin_password_again|密码' => 'require|min:6|max:20'
        ]);
        if(!$validate->check($params)){
            $errors = $validate->getError();
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        if($params['admin_password'] !== $params['admin_password_again']){
            $errors = "密码不一致，请重新输入";
            return View::fetch('public/errors',['errors'=>$errors]);
        }
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);
        $adminOne = AdminModel::where('admin_id',$params['admin_id'])->find();
        if(!($params['admin_password'] == $adminOne['admin_password'])){
            $adminOne['admin_password'] = $params['admin_password'];
        }
        $adminOne['admin_name'] = $params['admin_name'];
        $adminOne['admin_email'] = $params['admin_email'];
        $result = $adminOne->save();
        return View::fetch('public/tips',['result'=>$result,'name' => 'admin','str' =>'请返回管理员列表页面']);
    }
}
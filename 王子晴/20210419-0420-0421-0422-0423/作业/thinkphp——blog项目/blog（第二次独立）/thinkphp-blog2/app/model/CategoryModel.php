<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 8:23
 */
namespace app\model;

use think\Model;

class CategoryModel extends Model
{
    protected $name = 'category';
    protected $pk = 'category_id';

}
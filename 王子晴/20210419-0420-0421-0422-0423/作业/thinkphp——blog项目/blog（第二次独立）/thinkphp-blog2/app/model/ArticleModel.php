<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 9:56
 */

namespace app\model;


use think\Model;
use think\facade\Request;

class ArticleModel extends Model
{
    protected $name = 'article';
    protected $pk = 'article_id';

    public static function getList(){
        parse_str(Request::query(),$queryParams);
        $articleList = ArticleModel::paginate([
            'list_rows' => 2,
            'query' => $queryParams
        ]);
        foreach ($articleList as &$value){
            $categoryOne = CategoryModel::find($value['category_id']);
            $value['category_name'] = $categoryOne['category_name'];
        }
        unset($value);
        return $articleList;
    }
}
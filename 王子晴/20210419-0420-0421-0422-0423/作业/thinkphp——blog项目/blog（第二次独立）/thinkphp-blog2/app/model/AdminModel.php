<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/28
 * Time: 11:24
 */

namespace app\model;


use think\Model;

class AdminModel extends Model
{
    protected $name = 'admin';
    protected $pk = 'admin_id';
}
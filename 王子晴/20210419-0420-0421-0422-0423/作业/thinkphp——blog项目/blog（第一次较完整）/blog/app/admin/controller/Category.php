<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/22
 * Time: 9:24
 */
namespace app\admin\controller;
use app\model\CategoryModel;
use app\model\ArticleModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;
class Category{
    //分类列表
    public function index()
    {
        //使用模型查询分类列表
        parse_str(Request::query(),$queryParams);
//        var_dump($queryParams);exit();        //输出admin/category/index和页数
        $categoryList = CategoryModel::paginate([
            'list_rows' => 2,       //每页显示两条信息
            'query' => $queryParams     //实际上页面所有的get参数——输出admin/category/index和页数
        ]);
        //渲染视图
        return View::fetch('',['categoryList'=>$categoryList]);
    }
    //分类增加或编辑
    public function addOrEdit(){
        //获取参数id
        $categoryId = Request::param('category_id');
        if(!$categoryId){   //没有分类id参数，则是增加页面
            //渲染视图
            return View::fetch('category/add');
        }else{              //没有分类id参数，则是编辑页面
            //校验参数
            $validate = Validate::rule([
                'category_id|分类id'=>'require|between:1,'.PHP_INT_MAX
            ]);
            if(!$validate->check(['category_id'=>$categoryId])){
                View::assign('str',$validate->getError());
                return View::fetch('public/errors');
            }

            //使用模型查询一条信息
            $category = CategoryModel::where('category_id','=',$categoryId)->find();
            if (!$category){
                View::assign('str','分类不存在');
                return View::fetch('public/errors');
            }

            //渲染视图
            return View::fetch('category/edit',['category'=>$category]);
        }
    }

    //分类增加保存
    public function addSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'category_name|分类名称'=>'require|min:2|max:45',
            'category_desc|分类描述'=>'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }

        $params['add_time'] = time();
        $params['update_time'] = $params['add_time'];
        //使用模型插入一条信息
        $result = CategoryModel::create($params);

        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'category','str' => '返回分类列表页']);
    }

    //分类编辑保存
    public function editSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'category_id|分类id'=>'require|between:1,'.PHP_INT_MAX,
            'category_name|分类名称'=>'require|min:2|max:45',
            'category_desc|分类描述'=>'require|min:10|max:255'
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }

        //使用模型更新一条信息
            //查找信息
            $categoryOne = CategoryModel::where('category_id','=',$params['category_id'])->find();

            if(!$categoryOne){
                View::assign('str','分类不存在');
                return View::fetch('public/errors');
            }
            //更新数据
            $categoryOne['category_name'] = $params['category_name'];
            $categoryOne['category_desc'] = $params['category_desc'];
            $params['update_time'] = time();

            //保存信息
            $result = $categoryOne->save();
        //渲染视图
       return View::fetch('public/tips',['result'=>$result,'name'=>'category','str' => '返回分类列表页']);
    }
    //分类删除
    public function delete(){
        //获取参数：id
        $categoryId = Request::param('category_id');
        //校验参数
        $validate = Validate::rule([
            'category_id|分类id'=>'require|between:1,'.PHP_INT_MAX
        ]);
        if($validate->check(['categoryId'=>$categoryId])){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }

        //使用模型查询一条信息:分类下有文章
        $articleOne = ArticleModel::where('category_id','=',$categoryId)->find();
        if($articleOne){
            View::assign('str','分类下有文章，请先删除相关文章');
            return View::fetch('public/errors');
        }
        //使用模型删除一条信息
        $result = CategoryModel::destroy($categoryId);

        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'category','str' => '返回分类列表页']);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/22
 * Time: 20:24
 */
namespace app\admin\controller;
use app\model\AdminModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Admin{
    //管理员列表
    public function index(){

        parse_str(Request::query(),$queryParams);
        $adminInfoList = AdminModel::paginate([
            'list_rows' => 2,       //每页显示两条信息
            'query' => $queryParams     //实际上页面所有的get参数——输出admin/category/index和页数
        ]);
        //渲染视图
        return View::fetch('',['adminInfoList'=>$adminInfoList]);
    }
    //管理员增加
    public function add(){
        //渲染视图
        return View::fetch();
}
    //管理员增加保存
    public function addSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'admin_email|管理员邮箱' => 'require|min:5|max:100',
            'admin_name|管理员名称' => 'require|min:2|max:30',
            'admin_password|管理员密码' => 'require|min:6|max:20',
            'again_password|管理员密码' => 'require|min:6|max:20'
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        if(!($params['admin_password'] === $params['again_password'])){
            View::assign('str','密码不一致，请重新输入');
            return View::fetch('public/errors');
        }
        //对密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);
        //使用模型插入数据
        $result= AdminModel::create($params);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'admin','str' => '返回管理员列表页']);
    }
    //管理员编辑
    public function edit(){
        //获取参数：id
        $adminId = Request::param('admin_id');
        //校验参数
        $validate = Validate::rule([
           'admin_id|管理员id' => 'require|between:1,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['admin_id'=>$adminId])){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        //使用模型查询一条信息
        $adminInfo = AdminModel::where('admin_id','=',$adminId)->find();
        if(!$adminInfo){
            View::assign('str','管理员不存在');
            return View::fetch('public/errors');
        }
        //渲染视图
        return View::fetch('',['adminInfo'=>$adminInfo]);
    }
    //管理员编辑保存
    public function editSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate1 = Validate::rule([
            'admin_email|管理员邮箱' => 'require|min:5|max:100',
            'admin_name|管理员名称' => 'require|min:2|max:30',
            'admin_password|管理员密码' => 'require|min:6|max:20',
            'again_password|管理员密码' => 'require|min:6|max:20'
        ]);
        if(!$validate1->check($params)){
            View::assign('str',$validate1->getError());
            return View::fetch('public/errors');
        }

        if(!($params['admin_password'] === $params['again_password'])){
            View::assign('str','密码不一致，请重新输入');
            return View::fetch('public/errors');
        }

        //对密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);

        //使用模型更新一条信息
            //查找信息
            $admin = AdminModel::where('admin_email','=',$params['admin_email'])->find();
            //判断是否修改密码
            if($admin['admin_password'] !== $params['admin_password']){
                //更新信息
                $admin['admin_password'] = $params['admin_password'];
            }
                //更新信息
                $admin['admin_name'] = $params['admin_name'];
                $admin['update_time'] = time();
                $result = $admin->save();

        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'admin','str' => '返回管理员列表页']);
    }
    //管理员删除
    public function delete(){
        //获取参数：id
        $adminId = Request::param('admin_id');
        //校验参数
        $validate = Validate::rule([
            'admin_id|管理员id' => 'require|between:1,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['admin_id'=>$adminId])){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        //使用模型删除信息
        $result = AdminModel::destroy($adminId);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'admin','str' => '返回管理员列表页']);
    }

}
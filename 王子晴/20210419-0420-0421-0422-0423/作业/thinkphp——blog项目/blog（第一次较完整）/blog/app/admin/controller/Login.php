<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/23
 * Time: 9:51
 */

namespace app\admin\controller;
use app\model\AdminModel;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;
use think\Response;

class Login
{
    //登录
    public function index(){
        //渲染视图
        return View::fetch();

    }
    //登录保存
    public function loginSave(){
        session_start();
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'admin_email|管理员邮箱' => 'require|min:5|max:100',
            'admin_password|管理员密码' => 'require|min:6|max:20',
            'admin_randomNum|验证码' => 'require',
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        if(!($params['admin_randomNum'] == $_SESSION['checkNum'])){
            View::assign('str','验证码不一致，请重新输入');
            return View::fetch('public/errors');
        }
        //对密码进行md5加密
        $salt = "Yd2IFylMbZRwVVg8bbYwC@R#!#&)S";
        $params['admin_password'] = md5($salt.md5($salt.$params['admin_password'].$salt).$salt);

        //使用模型查询信息
        $result = AdminModel::where('admin_email','=',$params['admin_email'])->find();
        if(!($params['admin_password'] == $result['admin_password'])){
            View::assign('str','姓名或密码错误，请重新输入');
            return View::fetch('public/errors');
        }
        if($result){
            session('admin_email',$params['admin_email']);
            session('admin_name',$result['admin_name']);

            //点击了记住我，设置过期时间一天后
            if(empty($params['isRemember'])){

            }else{
                setcookie('PHPSESSID',session_id(),time()+10);
            }

        }
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'article','str' => '前往文章列表页']);
    }
    //登出
    public function logout(){

        session('admin_email',null);
        session('admin_name',null);


        $result = empty($_SESSION['admin_email']);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'login','str' => '返回登录页面']);
    }

    public function verifyCode()
    {
        session_start();
        //生成图片：148 * 50
        $image = imagecreate(148, 50);
        //背景色：白色
        $bgColor = imagecolorallocate($image, 255, 255, 255);
        //字体颜色：红51，绿108，蓝175
        $fontColor = imagecolorallocate($image, 51, 108, 175);
        //声明一个空变量，用于存放验证码
        $machineRandomNum = '';
        //循环六位随机数字
        for ($i = 1; $i <= 6; $i++) {
            $num = mt_rand(0, 9);
            imagestring($image, 6, 20 * $i, 20, $num, $fontColor);
            $machineRandomNum .= $num;    //把验证码的每一个值赋值给变量
        }
        $_SESSION['checkNum'] = $machineRandomNum;
        //画横线
        imageline($image, 12, 28, 135, 28, $fontColor);
        //输出到浏览器
        header("Content-Type:image/png");
        //生成图片
        \imagepng($image);
        //销毁一图像，释放资源
        \imagedestroy($image);

        exit();

    }
}
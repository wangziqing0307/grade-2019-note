<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/23
 * Time: 10:38
 */
namespace app\admin\middleware;
class Auth
{
    //登录验证
    public function handle($request,\Closure $next)
    {
        //用正则表达式筛选出不是login相关页面的页面——preg_match()匹配$request->pathinfo()中是否有/login/
        if(!preg_match("/login/",$request->pathinfo())){        //$request->pathinfo()——当前页面的类名/方法名——如果没有$request->pathinfo()，会有错误localhost 将您重定向的次数过多。
            //判断页面是否登录
            if(empty(session('admin_email'))){
                //无，则返回跳转到登录页面
                return redirect('/index.php?s=admin/login/index');
            }
        }

        $response = $next($request);
        return $response;
    }
}

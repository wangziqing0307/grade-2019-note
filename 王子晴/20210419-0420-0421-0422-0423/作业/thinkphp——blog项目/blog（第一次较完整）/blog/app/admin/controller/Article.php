<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/22
 * Time: 11:06
 */
namespace app\admin\controller;
use app\model\ArticleModel;
use app\model\CategoryModel;
use think\db\Query;
use think\facade\Request;
use think\facade\Validate;
use think\facade\View;

class Article{
    //文章列表
    public function index(){

        $articleList = ArticleModel::getList();

        //渲染视图
        return View::fetch('',['articleList'=>$articleList]);
    }
    //文章增加或编辑
    public function addOrEdit(){
        //获取参数：id
        $articleId = Request::param('article_id');
        if(!$articleId){
            //使用模型查询分类名称
            $categoryList = CategoryModel::select();
            //渲染视图
            return View::fetch('add',['categoryList'=>$categoryList]);
        }else{
            //校验参数
            $validate = Validate::rule([
                'article_id|文章id' => 'require|between:1,'.PHP_INT_MAX
            ]);
            if(!$validate->check(['article_id'=>$articleId])){
                View::assign('str',$validate->getError());
                return View::fetch('public/errors');
            }

            //使用模型查询一条信息
            $article = ArticleModel::where('article_id','=',$articleId)->find();
            if(!$article){
                View::assign('str','文章不存在');
                return View::fetch('public/errors');
            }

            //使用模型查询分类表
            $categoryList = CategoryModel::select();

            //渲染视图
            return View::fetch('edit',['article'=>$article,'categoryList'=>$categoryList]);
        }
    }

    //文章增加保存
    public function addSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'article_title|文章标题' => 'require|min:5|max:50',
            'category_id|文章分类' => 'require|between:1,'.PHP_INT_MAX,
            'article_author|文章作者' => 'require',
            'article_content|文章内容' => 'require|min:10|max:8000'
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        $params['add_time'] = time();
        $params['update_time'] = $params['add_time'];
        //使用模型增加一条信息
        $result = ArticleModel::create($params);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'article','str' => '返回文章列表页']);
    }

    //文章编辑保存
    public function editSave(){
        //获取参数
        $params = Request::param();
        //校验参数
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,'.PHP_INT_MAX,
            'article_title|文章标题' => 'require|min:5|max:50',
            'category_id|文章分类' => 'require|between:1,'.PHP_INT_MAX,
            'article_author|文章作者' => 'require',
            'article_content|文章内容' => 'require|min:10|max:8000'
        ]);
        if(!$validate->check($params)){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        //使用模型更新数据
            //查找信息
            $articleOne = ArticleModel::where('article_id','=',$params['article_id'])->find();
            if(!$articleOne){
                echo "文章不存在<br/><a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
                exit();
            }
            //更新数据
            $articleOne['article_title'] = $params['article_title'];
            $articleOne['category_id'] = $params['category_id'];
            $articleOne['article_author'] = $params['article_author'];
            $articleOne['article_content'] = $params['article_content'];
            $articleOne['update_time'] = time();
            //保存数据
            $result = $articleOne->save();
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'article','str'=>'返回文章列表页']);
    }
    //文章删除
    public function delete(){
        //获取参数
        $articleId = Request::param('article_id');
        //校验参数
        $validate = Validate::rule([
            'article_id|文章id' => 'require|between:1,'.PHP_INT_MAX
        ]);
        if(!$validate->check(['article_id'=>$articleId])){
            View::assign('str',$validate->getError());
            return View::fetch('public/errors');
        }
        //使用模型删除信息
        $result = ArticleModel::destroy($articleId);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'article','str'=>'返回文章列表页']);
    }
    //文章批量删除
    public function deleteMulti(){
        //获取参数
        $articleIds = Request::param('article');
        //校验参数
        foreach ($articleIds as $value){
            $validate = Validate::rule([
               "article_id|文章id" => 'require|between:1,'.PHP_INT_MAX
           ]);
            if(!$validate->check(['article_id'=>$value])){
                View::assign('str',$validate->getError());
                return View::fetch('public/errors');
            }
        }
        //使用模型批量删除信息
        $result = ArticleModel::destroy($articleIds);
        //渲染视图
        return View::fetch('public/tips',['result'=>$result,'name'=>'article','str'=>'返回文章列表页']);
    }
}
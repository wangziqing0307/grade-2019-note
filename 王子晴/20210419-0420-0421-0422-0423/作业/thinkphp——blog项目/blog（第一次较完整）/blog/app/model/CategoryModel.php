<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/22
 * Time: 9:22
 */
namespace app\model;
class CategoryModel extends \think\Model{
    protected $name = 'category';
    protected $pk = 'category_id';
}
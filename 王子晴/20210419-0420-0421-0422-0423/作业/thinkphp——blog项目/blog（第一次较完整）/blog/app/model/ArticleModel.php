<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/4/22
 * Time: 11:02
 */
namespace app\model;
use think\facade\Request;

class ArticleModel extends \think\Model{
    protected $name = 'article';
    protected $pk = 'article_id';

    public static function getList($getCategoryName = true){

        //使用模型查询文章列表
        parse_str(Request::query(),$queryParams);
        $articleList = ArticleModel::paginate([
            'list_rows' => 2,
            'query' => $queryParams
        ]);

        if($getCategoryName){
            foreach ($articleList as &$row){
                $category = CategoryModel::find($row['category_id']);
                $row['category_name'] = $category['category_name'];
            }
            unset($row);
        }
        return $articleList;
    }
}
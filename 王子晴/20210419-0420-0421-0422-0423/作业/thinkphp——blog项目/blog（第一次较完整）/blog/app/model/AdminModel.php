<?php
/**
 * Created by PhpStorm.
 * User: WZQ
 * Date: 2021/4/22
 * Time: 20:25
 */

namespace app\model;

use think\Model;

class AdminModel extends Model
{
    protected $name = 'admininfo';
    protected $pk = 'admin_id';
}
create database if not exists blog
	DEFAULT CHARACTER SET utf8mb4
	DEFAULT COLLATE utf8mb4_general_ci;
use blog;

CREATE TABLE `category` (
  `category_id` int NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `category_name` varchar(20) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(1000) NOT NULL COMMENT '分类描述',
  `add_time` int unsigned NOT NULL COMMENT '增加时间',
  `update_time` int unsigned NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `article` (
  `article_id` int NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `article_title` varchar(100) NOT NULL COMMENT '文章标题',
  `category_id` int NOT NULL COMMENT '文章类别，分类id',
  `article_author` varchar(20) DEFAULT NULL COMMENT '文章作者',
  `article_content` text COMMENT '文章内容',
  `add_time` int unsigned NOT NULL COMMENT '增加时间',
  `update_time` int NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `admininfo` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `admin_name` varchar(20) NOT NULL COMMENT '用户名',
  `admin_password` varchar(30) NOT NULL COMMENT '用户密码',
  `add_time` int(11) unsigned NOT NULL COMMENT '增加时间s',
  `update_time` int(11) unsigned NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
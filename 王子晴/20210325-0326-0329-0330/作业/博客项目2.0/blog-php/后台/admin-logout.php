<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/29
 * Time: 16:50
 */
//开启会话
session_start();

//删除session
unset($_SESSION['admin_email']);
unset($_SESSION['admin_name']);

echo "登出成功，<br/>请<a href='admin-login.php'>返回登录页面</a>";
echo exit();
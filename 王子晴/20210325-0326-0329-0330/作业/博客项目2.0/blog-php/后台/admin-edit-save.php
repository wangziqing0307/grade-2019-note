<?php
/**
 * 后台——编辑管理员保存页面
 */
//接收编辑管理员信息
$adminEmail = $_POST['admin_email'];
$adminName = $_POST['admin_name'];
$adminPassword = $_POST['admin_password'];
$againPassword = $_POST['again_password'];

//姓名2~30长度
if(mb_strlen($adminName)<2 || mb_strlen($adminName)>30){
    echo "姓名长度2~30<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//密码6~20长度
if(mb_strlen($adminPassword)<6 || mb_strlen($adminPassword)>20){
    echo "密码长度6~20<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//判断密码是否一致
if(!($adminPassword == $againPassword)){
    echo "密码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}


//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条管理员信息到数据库
$sql = "update adminInfo 
          set admin_name = '$adminName',admin_password = '$adminPassword',update_time = '$updateTime'
          where admin_email = '$adminEmail'";
$result = $db->exec($sql);

//判断是否编辑管理员信息成功
if($result){
    echo '编辑管理员信息成功<br/><a href="admin-list.php">返回文章列表页</a>';
    exit();
}else{
    echo '编辑管理员信息失败，错误信息为'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}

<?php
/**
 *管理员登录保存页面
 */

//接收到管理员登录信息
$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$isRemember = $_POST['isRemember'] ?? '';
$adminNum = $_POST['admin_randomNum'];

//判断验证码是否一致
session_start();

if(!($adminNum == $_SESSION['checkNum'])){
    echo "验证码不一致，请重新输入<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "wzq0307.");
$db -> exec("set names utf8mb4");

//查询该用户是否存在且信息正确
$sql = "select * from adminInfo where admin_email = '$adminEmail'";
$result = $db->query($sql);
$adminInfo = $result->fetch(PDO::FETCH_ASSOC);

if($adminInfo && $adminInfo['admin_password'] == $adminPassword){
    //登录成功后，设置相关信息
    //开启会话
    session_start();
    $_SESSION['admin_email'] = $adminEmail;
    $_SESSION['admin_name'] = $adminInfo['admin_name'];
    //点击了记住我，设置过期时间一天后
    if($isRemember){
       setcookie('PHPSESSID',session_id(),time()+10);
    }
    echo "登录成功<br/>";
    echo "<a href='article-list.php'>前往文章列表页</a>";
    exit();
}else{
    echo "姓名或密码错误，请重新输入<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
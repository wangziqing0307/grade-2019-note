<?php
/**后台——编辑文章保存页面
 */
//获取到文章信息
$articleId = $_POST['article_id'];
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$articleAuthor = $_POST['article_author'];
$articleContent = $_POST['article_content'];

//表单验证(后台管理功能细节)
//文章id不能为空
if(empty($articleId)){
    echo "文章id不能为空<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//文章标题5到50个字
if(mb_strlen($articleTitle)<5 || mb_strlen($articleTitle)>50){
    echo "文章标题5-50个字<br/>";
    echo '<a href="javascript:void(0)" onclick="history.back()">返回上一页</a>';
    exit();
}
//文章作者必须填写
if(empty($articleAuthor)){
    echo "文章作者不能为空<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}
//文章内容必须填写，10-8000个字
if(mb_strlen($articleContent)<10 || mb_strlen($articleContent)>8000){
    echo "文章内容10~8000个字<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

$updateTime = time();

//修改一条文章信息到数据库
$sql = "update article 
          set article_title = '$articleTitle',category_id = '$categoryId',article_author = '$articleAuthor',article_content = '$articleContent',update_time = '$updateTime'
          where article_id = '$articleId'";
$result = $db->exec($sql);

//判断是否编辑文章信息成功
if($result){
    echo '编辑文章信息成功<br/><a href="article-list.php">返回文章列表页</a>';
    exit();
}else{
    echo '编辑文章信息失败，错误信息为'.$db->errorInfo()[2].'。请联系管理员：2270467108@qq.com';
}
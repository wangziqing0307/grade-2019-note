<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/30
 * Time: 15:58
 */
//生成图片：148 * 50
$image = imagecreate(148,50);
//背景色：白色
$bgColor = imagecolorallocate($image,255,255,255);
//字体颜色：红51，绿108，蓝175
$fontColor = imagecolorallocate($image,51,108,175);
//声明一个空变量，用于存放验证码
$machineRandomNum = '';
//循环六位随机数字
for($i = 1; $i <= 6 ;$i++){
    $num = mt_rand(0,9);
    imagestring($image,6,20 * $i,20,$num,$fontColor);
    $machineRandomNum.=$num;    //把验证码的每一个值赋值给变量
}
//画横线
imageline($image,12,28,135,28,$fontColor);
//输出到浏览器
header("Content-Type:image/png");
//生成图片
imagepng($image);

session_start();
$_SESSION['checkNum'] = $machineRandomNum;
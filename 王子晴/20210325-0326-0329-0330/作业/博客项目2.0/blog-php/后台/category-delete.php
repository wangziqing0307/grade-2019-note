<?php
/**
后台——分类删除
 */
//获取分类id
$categoryId = $_GET['category_id'];

//表单验证(后台管理功能细节)
//分类id不能为空
if(empty($categoryId)){
    echo "分类id不能为空<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//表单验证(后台管理功能细节)
//点击删除，如果分类下有文章，那么提示：分类下有文章，请先删除相关文章
$articleSql = "select * from article where category_id = '$categoryId' limit 1";
$articleResult = $db->query($articleSql);
$articleList = $articleResult->fetchAll(PDO::FETCH_ASSOC);

if(!empty($articleList)){
    echo "分类下有文章，请先删除相关文章<br/>";
    echo "<a href='javascript:void(0)' onclick='history.back()'>返回上一页</a>";
    exit();
}

$addTime = time();
$updateTime = $addTime;

//删除一条分类信息到数据库
$sql = "delete from category where category_id = '$categoryId'";
$result = $db->exec($sql);

//判断是否删除分类信息成功
if($result){
    echo '删除分类信息成功<br/><a href="category-list.php">返回分类列表首页</a>';
    exit();
}else{
    echo '删除分类信息失败，错误信息：'.$db->errorInfo()[2].'.请联系管理员：2270467108@qq.com';
}
<?php
/**
后台——文章列表
 */

//开启会话
session_start();

if(empty($_SESSION['admin_email'])){
    echo "尚未登录，<br/>请<a href='admin-login.php'>前往登录页面</a>,重新登录";
    exit();
}

//设置时区
date_default_timezone_set("PRC");

//连接MySQL数据库
$dsn = "mysql:host=localhost;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db -> exec("set names utf8mb4");

//查询表article
$sql = "select * from article order by article_id asc ";
$result = $db->query($sql);
//var_dump($db->errorInfo());
//exit();
$articleList = $result->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>后台——文章列表</title>
    <link rel="stylesheet" type="text/css" href="./css/behind-main.css" />
</head>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <div id="admin-info">欢迎你：<?php echo $_SESSION['admin_name'] ?> &nbsp;<a href="admin-logout.php">退出登录</a></div>
    </div>
    <div id="left">
        <ul>
            <li><a href="category-list.php">分类管理</a></li>
            <li><a href="article-list.php">文章管理</a></li>
            <li><a href="admin-list.php">管理员</a></li>
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article-list.php">首页</a>&gt;
                <a href="article-list.php">文章管理</a>&gt;
                <a href="article-list.php">文章列表</a>
            </div>
            <div id="table-menu">
<!--                <button class="btn">全选</button>-->
<!--                <a href="#">删除选中任务</a>-->
                <a href="article-add.php" id="add_article">增加文章</a>
            </div>
            <div class="table-list" id="list">
                <form action="#" method="post">
                    <table>
                        <tr>
                            <th id="fist_th"></th>
                            <th>文章id</th>
                            <th>文章标题</th>
                            <th>文章分类</th>
                            <th>文章作者</th>
                            <th>发表时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        <?php foreach ($articleList as $item): ?>
                        <tr>
                            <td><input type="checkbox" id="table_checkbox" /></td>
                            <td><?php echo $item['article_id'] ?></td>
                            <td><?php echo $item['article_title'] ?></td>
                            <td><?php
                                    $sql = "select * from category where category_id='{$item['category_id']}'";
                                    $result = $db -> query($sql);
                                    $category = $result -> fetch(PDO::FETCH_ASSOC);
                                    echo $category['category_name'];
                                ?></td>
                            <td><?php echo $item['article_author'] ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$item['add_time']) ?></td>
                            <td><?php echo date("Y-m-d H:i:s",$item['update_time']) ?></td>
                            <td>
                                <a href="article-edit.php?article_id=<?php echo $item['article_id'] ?>">编辑</a>
                                <a href="article-delete.php?article_id=<?php echo $item['article_id'] ?>">删除</a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<body>
</body>
</html>


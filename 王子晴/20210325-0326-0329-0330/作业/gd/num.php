<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/30
 * Time: 15:49
 */
$image = imagecreate(200,100);
$bgColor = imagecolorallocate($image,186,85,211);
$fontColor = imagecolorallocate($image,0,0,0);

for($i = 0;$i < 4;$i ++){
    $num = mt_rand(0,9);
    imagestring($image,6,20 * ($i + 1),20,$num,$fontColor);
}
imageline($image,11,28,98,28,$fontColor);

header("Content-Type:image/png");

//生成图片
imagepng($image);

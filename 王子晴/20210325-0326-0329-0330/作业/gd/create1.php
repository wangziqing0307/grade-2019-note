<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/30
 * Time: 14:28
 */
//1. 完成课堂上讲解的习题。
    //创建一张图片：100 * 100
    $image = imagecreate(100,100);
    //背景色：粉色pink
    $bgColor = imagecolorallocate($image,255,192,203);
    //字体颜色：黑色
    $fontColor = imagecolorallocate($image,0,0,0);
    //1-1、写上一个数字5
//    imagestring($image,4,5,5,5,$fontColor);
//        //生成图片
//        imagepng($image,'exercise1-1.png');
    //1-2、写上0~9随机数字
//    $num = mt_rand(0,9);
//    imagestring($image,4,10,10,$num,$fontColor);
//        //生成图片
//        imagepng($image,"exercise1-2.png");
    //1-3、写上4个随机数字
    //法一（推荐）（安全性高）
//    for($i = 1;$i <= 4;$i++){
//        $num = mt_rand(0,9)
//        imagestring($image,4,10 * $i,10,$num,$fontColor);
//    }
//        //生成图片
//        imagepng($image,'exercise1-3-1.png');
    //法二
//    $num = mt_rand(1000,9999);
//    imagestring($image,4,10,10,$num,$fontColor);
//        //生成图片
//        imagepng($image,'exercise1-3-2.png');
    //1-4、可以支持A-Za-z随机字母
    $str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    // 10 + 26 + 26 -1 = 61
//    echo $str[61];
    $max = mb_strlen($str) - 1;
//    for($i = 1 ;$i <= 4;$i ++){
//        $num = mt_rand(0,$max);
//        imagestring($image,4,10 * $i,10,$str[$num],$fontColor);
//    }
//        //生成图片
//        imagepng($image,"exercise1-4.png");
    //销毁图片
    imagedestroy($image);
echo "success";
//2. 了解png、jpg、gif图片格式，并写下你的理解。
    //JPG：一种常用于摄影作品或写实作品、有损压缩图形格式（不支持动图）
    //PNG：一种清晰、支持透明、无损压缩的位图图形格式（不支持动图）
    //GIF：一种色彩效果低、体积小、支持透明的图像文件格式（不支持动图，有动画GIF，还有静态GIF）
    //图片大小比较：PNG ≈ JPG > GIF
    //网页兼容程度：GIF ≈ JPG > PNG
    //图形透明性：PNG > GIF > JPG
    //色彩丰富程度：JPG > PNG （png24 > png8）> GIF
    //单调色彩图片：GIF > PNG > JPG
//3. 了解三原色是什么，并写下你的理解。
    //三原色，是色彩三原色以及光学三原色（red，green，blue）（RGB）
    //三原色光的发现：发现光的分解合成的奥妙后，牛顿在思考是不是七色光还能再分解，通过无数次的试验，
    //              牛顿发现红绿蓝三种色光是不能再分解的，其他四种色光都是这三种光线按不同比例混合得来的，于是红绿蓝被称为色光三原色。
    //红光 + 绿光 = 黄光；  互补与蓝光
    //红光 + 蓝光 = 品红色；互补与绿光
    //绿光 + 蓝光 = 青光；  互补与红光
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/30
 * Time: 14:29
 */
$image = imagecreate(200,100);
$bgColor = imagecolorallocate($image,186,85,211);
$fontColor = imagecolorallocate($image,0,0,0);
//4. 对课堂的验证码例子进行改进，改进功能如下：
//	1. 除了0-9数字，还可以支持a-z、A-Z的字符。
$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
$max = mb_strlen($str) - 1;
//for($i = 0;$i < 4;$i++){
//    $num = mt_rand(0,$max);
//    imagestring($image,4,10 * ($i + 1),10,$str[$num],$fontColor);
//}
//    //生成图片
//    imagejpeg($image,'exercise4-1.jpeg');
//	2. 生成6位随机字符。
//for($i = 0 ;$i < 6;$i++){
//    $num = mt_rand(0,9);
//    imagestring($image,4,20 * ($i + 1),20,$num,$fontColor);
//}
//    //生成图片
//    imagejpeg($image,"exercise4-2.jpeg");
//	3. 每个数字的颜色随机生成。
//for($i = 0;$i < 6;$i++){
//    $red = mt_rand(0,255);
//    $green = mt_rand(0,255);
//    $blue = mt_rand(0,255);
//    $fontColor2 = imagecolorallocate($image,$red,$green ,$blue);
//    $num = mt_rand(0,9);
//    imagestring($image,5,20 * ($i + 1),20,$num,$fontColor2);
//}
//    //生成图片
//    imagepng($image,'exercise4-3.png');
//	4. 生成随机雪花，提示：雪花可以用*
//法一
$j = mt_rand(0,100);
$fontColor3 = imagecolorallocate($image,255,255,255);
//for($i = 0;$i < $j;$i++){
//    imagestring($image,6,20 * ($i + 1),20,'*',$fontColor3);
//}
//    //生成图片
//    imagepng($image,'exercise4-4.png');

//法二
$j = mt_rand(0,20);
$fontColor3 = imagecolorallocate($image,255,255,255);
for($i = 0;$i < $j;$i++){
    imagestring($image,6,mt_rand(0,20),mt_rand(0,20),"*",$fontColor3);
}
//生成图片
imagepng($image,'exercise4-4-2.png');

//5. 尝试在验证码中间进行划线。
//for($i = 0;$i < 4;$i ++){
//    $num = mt_rand(0,9);
//    imagestring($image,6,20 * ($i + 1),20,$num,$fontColor);
//}
//imageline($image,11,28,98,28,$fontColor);
//    //生成图片
//    imagepng($image,'exercise4-5.png');


//销毁图片
imagedestroy($image);

echo "success";